package com.fams.syllabus_repository.Service.Impl;

import com.fams.syllabus_repository.dto.TrainingContentDto;
import com.fams.syllabus_repository.dto.TrainingUnitDto;
import com.fams.syllabus_repository.dto.TrainingUnitListDto;
import com.fams.syllabus_repository.entity.DaySyllabus;
import com.fams.syllabus_repository.entity.TrainingContent;
import com.fams.syllabus_repository.entity.TrainingUnit;
import com.fams.syllabus_repository.enums.DeliveryType;
import com.fams.syllabus_repository.exceptions.TrainingUnitNotFoundException;
import com.fams.syllabus_repository.repository.DaySyllabusRepository;
import com.fams.syllabus_repository.repository.SyllabusRepository;
import com.fams.syllabus_repository.repository.TrainingUnitRepository;
import com.fams.syllabus_repository.service.TrainingUnitService;
import com.fams.syllabus_repository.service.impl.TrainingUnitServiceImpl;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class TrainingUnitServiceImplTest {
    @Mock
    private SyllabusRepository syllabusRepository;
    @Mock
    private TrainingUnitRepository trainingUnitRepository;
    @Mock
    private DaySyllabusRepository daySyllabusRepository;
    @InjectMocks
    private TrainingUnitServiceImpl trainingUnitService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetTrainingUnitBySyllabusId() {
        // Arrange
        Long syllabusId = 1L;

        TrainingUnit trainingUnit1 = new TrainingUnit();
        trainingUnit1.setId(1L);
        trainingUnit1.setUnitName("Unit 1");
        trainingUnit1.setUnitNumber("001");
        trainingUnit1.setDaySyllabus(new DaySyllabus());

        TrainingUnit trainingUnit2 = new TrainingUnit();
        trainingUnit2.setId(2L);
        trainingUnit2.setUnitName("Unit 2");
        trainingUnit2.setUnitNumber("002");
        trainingUnit2.setDaySyllabus(new DaySyllabus());

        List<TrainingUnit> trainingUnits = Arrays.asList(trainingUnit1, trainingUnit2);

        TrainingContent trainingContent1 = new TrainingContent();
        trainingContent1.setId(1L);
        trainingContent1.setName("Content 1");
        trainingContent1.setDeliveryType(DeliveryType.Exam);
        trainingContent1.setTrainingTime(60);
        trainingContent1.setMethod("Method 1");

        TrainingContent trainingContent2 = new TrainingContent();
        trainingContent2.setId(2L);
        trainingContent2.setName("Content 2");
        trainingContent2.setDeliveryType(DeliveryType.Quiz);
        trainingContent2.setTrainingTime(90);
        trainingContent2.setMethod("Method 2");

        trainingUnit1.setTrainingContents(Arrays.asList(trainingContent1));
        trainingUnit2.setTrainingContents(Arrays.asList(trainingContent2));

        Mockito.when(trainingUnitRepository.findByDaySyllabusId(syllabusId)).thenReturn(trainingUnits);

        // Act
        List<TrainingUnitListDto> result = trainingUnitService.getTrainingUnitBySyllabusId(syllabusId);

        // Assert
        Assert.assertEquals(2, result.size());

        TrainingUnitListDto unitDto1 = result.get(0);
        Assert.assertEquals(trainingUnit1.getId(), unitDto1.getId());
        Assert.assertEquals(trainingUnit1.getUnitName(), unitDto1.getUnitName());
        Assert.assertEquals(trainingUnit1.getUnitNumber(), unitDto1.getUnitNumber());
        Assert.assertEquals(1, unitDto1.getTrainingContent().size());

        TrainingContentDto contentDto1 = unitDto1.getTrainingContent().get(0);
        Assert.assertEquals(trainingContent1.getId(), contentDto1.getId());
        Assert.assertEquals(trainingContent1.getName(), contentDto1.getName());
        Assert.assertEquals(trainingContent1.getDeliveryType().name(), contentDto1.getDeliveryType());
        Assert.assertEquals(trainingContent1.getTrainingTime(), contentDto1.getTrainingTime());
        Assert.assertEquals(trainingContent1.getMethod(), contentDto1.getMethod());

        TrainingUnitListDto unitDto2 = result.get(1);
        Assert.assertEquals(trainingUnit2.getId(), unitDto2.getId());
        Assert.assertEquals(trainingUnit2.getUnitName(), unitDto2.getUnitName());
        Assert.assertEquals(trainingUnit2.getUnitNumber(), unitDto2.getUnitNumber());
        Assert.assertEquals(1, unitDto2.getTrainingContent().size());

        TrainingContentDto contentDto2 = unitDto2.getTrainingContent().get(0);
        Assert.assertEquals(trainingContent2.getId(), contentDto2.getId());
        Assert.assertEquals(trainingContent2.getName(), contentDto2.getName());
        Assert.assertEquals(trainingContent2.getDeliveryType().name(), contentDto2.getDeliveryType());
        Assert.assertEquals(trainingContent2.getTrainingTime(), contentDto2.getTrainingTime());
        Assert.assertEquals(trainingContent2.getMethod(), contentDto2.getMethod());

        Mockito.verify(trainingUnitRepository).findByDaySyllabusId(syllabusId);
    }

    @Test
    public void testCreateUnit() {
        // Arrange
        Long daySyllabusId = 1L;

        TrainingUnitDto trainingUnitDto = new TrainingUnitDto();
        trainingUnitDto.setId(2L);
        trainingUnitDto.setUnitName("Unit 1");
        trainingUnitDto.setUnitNumber("001");
        DaySyllabus daySyllabus = new DaySyllabus();
        daySyllabus.setId(daySyllabusId);
        TrainingUnit trainingUnit = new TrainingUnit();
        trainingUnit.setId(2L);
        trainingUnit.setUnitName("Unit 1");
        trainingUnit.setUnitNumber("001");
        trainingUnit.setDaySyllabus(daySyllabus);

        Mockito.when(daySyllabusRepository.findById(daySyllabusId)).thenReturn(Optional.of(daySyllabus));
        Mockito.when(trainingUnitRepository.save(trainingUnit)).thenReturn(trainingUnit);

        // Act
        TrainingUnitDto result = trainingUnitService.createUnit(daySyllabusId, trainingUnitDto);

        // Assert
        Assert.assertEquals(trainingUnitDto.getId(), result.getId());
        Assert.assertEquals(trainingUnitDto.getUnitName(), result.getUnitName());
        Assert.assertEquals(trainingUnitDto.getUnitNumber(), result.getUnitNumber());

        Mockito.verify(trainingUnitRepository).save(Mockito.any(TrainingUnit.class));
    }

    @Test
    public void testDeleteTrainingUnitById() {
        // Arrange
        Long unitId = 1L;

        // Create a TrainingUnit
        TrainingUnit trainingUnit = new TrainingUnit();
        trainingUnit.setId(unitId);

        Mockito.when(trainingUnitRepository.findById(unitId)).thenReturn(Optional.of(trainingUnit));

        // Act
        trainingUnitService.deleteTrainingUnitById(unitId);

        // Assert
        Mockito.verify(trainingUnitRepository).delete(trainingUnit);
    }

    @Test
    public void testDeleteTrainingUnitById_ThrowsNotFoundException() {
        // Arrange
        Long unitId = 1L;

        Mockito.when(trainingUnitRepository.findById(unitId)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(TrainingUnitNotFoundException.class, () -> {
            trainingUnitService.deleteTrainingUnitById(unitId);
        });
    }


}
