package com.fams.syllabus_repository.Service.Impl;

import com.fams.syllabus_repository.dto.TrainingContentDto;
import com.fams.syllabus_repository.entity.TrainingContent;
import com.fams.syllabus_repository.entity.TrainingUnit;
import com.fams.syllabus_repository.enums.DeliveryType;
import com.fams.syllabus_repository.enums.OutputStandard;
import com.fams.syllabus_repository.exceptions.TrainingContentNotFoundException;
import com.fams.syllabus_repository.exceptions.TrainingUnitNotFoundException;
import com.fams.syllabus_repository.repository.TrainingContentRepository;
import com.fams.syllabus_repository.repository.TrainingUnitRepository;
import com.fams.syllabus_repository.service.impl.TrainingContentServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class TrainingContentServiceImplementTest {
    @InjectMocks
    private TrainingContentServiceImpl trainingContentService;
    @Mock
    private TrainingContentRepository trainingContentRepository;
    @Mock
    private TrainingUnitRepository trainingUnitRepository;
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllTrainingContent() {
        List<TrainingContent> trainingContents = new ArrayList<>();
        TrainingContent trainingContent = new TrainingContent();
        trainingContent.setDeliveryType(DeliveryType.Exam);
        trainingContent.setOutputStandard(List.of(OutputStandard.C3SD));
        trainingContents.add(trainingContent);

        List<TrainingContentDto> expectedDtos = new ArrayList<>();
        TrainingContentDto dto  = new TrainingContentDto();
        dto.setDeliveryType(DeliveryType.Exam.name());
        dto.setOutputStandard(List.of(OutputStandard.C3SD));
        expectedDtos.add(dto);

        when(trainingContentRepository.findAll()).thenReturn(trainingContents);


        List<TrainingContentDto> actualDtos = trainingContentService.getAllTraniningContent();


        assertEquals(expectedDtos.size(), actualDtos.size());
        assertEquals(expectedDtos, actualDtos);
    }

    @Test
    public void testCreateContent() {
        // Arrange
        Long trainingUnitId = 1L;
        TrainingContentDto trainingContentDto = new TrainingContentDto();
        trainingContentDto.setName("Training Content");
        trainingContentDto.setDeliveryType(DeliveryType.Exam.name());

        TrainingContent trainingContent = new TrainingContent();
        trainingContent.setDeliveryType(DeliveryType.Exam);
        trainingContent.setName(trainingContentDto.getName());
        // Set other properties of trainingContent

        TrainingUnit trainingUnit = new TrainingUnit();
        trainingUnit.setId(trainingUnitId);

        when(trainingUnitRepository.findById(trainingUnitId)).thenReturn(Optional.of(trainingUnit));
        when(trainingContentRepository.save(any(TrainingContent.class))).thenReturn(trainingContent);

        // Act
        TrainingContentDto result = trainingContentService.createContent(trainingUnitId, trainingContentDto);

        // Assert
        verify(trainingUnitRepository, times(1)).findById(trainingUnitId);
        verify(trainingContentRepository, times(1)).save(any(TrainingContent.class));

        assertEquals(trainingContentDto.getName(), result.getName());
        // Assert other properties of result
    }

    @Test
    public void testCreateContent_NotFound() {
        // Arrange
        Long trainingUnitId = 1L;
        TrainingContentDto trainingContentDto = new TrainingContentDto();
        trainingContentDto.setOutputStandard(List.of(OutputStandard.C3SD));
        trainingContentDto.setDeliveryType(DeliveryType.Assignment.name());
        // Set other properties of trainingContentDto

        when(trainingUnitRepository.findById(trainingUnitId)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(TrainingUnitNotFoundException.class, () -> {
            trainingContentService.createContent(trainingUnitId, trainingContentDto);
        });
    }

    @Test
    public void testDeleteTrainingContentById() {
        // Arrange
        Long contentId = 1L;
        TrainingContent trainingContent = new TrainingContent();
        trainingContent.setId(contentId);

        when(trainingContentRepository.findById(contentId)).thenReturn(Optional.of(trainingContent));

        // Act
        trainingContentService.deleteTrainingContentById(contentId);

        // Assert
        verify(trainingContentRepository, times(1)).findById(contentId);
        verify(trainingContentRepository, times(1)).delete(trainingContent);
    }

    @Test
    public void testDeleteTrainingContentById_ThrowsException() {
        // Arrange
        Long contentId = 1L;

        when(trainingContentRepository.findById(contentId)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(TrainingContentNotFoundException.class, () -> {
            trainingContentService.deleteTrainingContentById(contentId);
        });

        verify(trainingContentRepository, times(1)).findById(contentId);
        verify(trainingContentRepository, never()).delete(any(TrainingContent.class));
    }
}
