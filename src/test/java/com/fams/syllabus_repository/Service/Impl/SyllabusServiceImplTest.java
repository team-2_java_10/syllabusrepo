package com.fams.syllabus_repository.Service.Impl;

import com.fams.syllabus_repository.dto.PagingSyllabusDto;
import com.fams.syllabus_repository.dto.SyllabusDetailDto;
import com.fams.syllabus_repository.dto.SyllabusDto;
import com.fams.syllabus_repository.entity.*;
import com.fams.syllabus_repository.enums.DeliveryType;
import com.fams.syllabus_repository.enums.OutputStandard;
import com.fams.syllabus_repository.enums.PublicStatus;
import com.fams.syllabus_repository.exceptions.BadRequestException;
import com.fams.syllabus_repository.exceptions.SyllabusNotFoundException;
import com.fams.syllabus_repository.repository.*;
import com.fams.syllabus_repository.request.RequestIdDto;
import com.fams.syllabus_repository.s3.S3Bucket;
import com.fams.syllabus_repository.s3.S3Sevice;
import com.fams.syllabus_repository.service.impl.SyllabusServiceImpl;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.opentest4j.AssertionFailedError;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.mock.web.MockMultipartFile;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;



@RequiredArgsConstructor
public class SyllabusServiceImplTest {
    @InjectMocks
    private SyllabusServiceImpl syllabusService;

    @Mock
    private SyllabusRepository repository;

    @Mock
    private DaySyllabusRepository daySyllabusRepository;

    @Mock
    private AssignmentShemeRepository assignmentShemeRepository;

    @Mock
    private TrainingUnitRepository trainingUnitRepository;

    @Mock
    private TrainingContentRepository trainingContentRepository;

    @Mock
    private OtherSyllabusRepository otherSyllabusRepository;

    @Mock
    private TrainingMaterialsRepositoty trainingMaterialsRepository;

    @Mock
    private S3Sevice s3Service;
    @Mock
    private S3Bucket s3Bucket;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCreateAndUpdate_ExistingSyllabusWithNonNullSyllabusName() {
        // Arrange
        Long existingSyllabusId = 1L;
        String syllabusName = "Sample Syllabus";

        SyllabusDto syllabusDto = new SyllabusDto();
        syllabusDto.setId(existingSyllabusId);
        syllabusDto.setSyllabusName(syllabusName);
        syllabusDto.setPublicStatus(PublicStatus.ACTIVE.getValue());

        Syllabus existingSyllabus = new Syllabus();
        existingSyllabus.setId(existingSyllabusId);
        existingSyllabus.setPublicStatus(PublicStatus.ACTIVE.getValue());

        Syllabus updatedSyllabus = new Syllabus();
        updatedSyllabus.setId(existingSyllabusId);
        updatedSyllabus.setPublicStatus(PublicStatus.ACTIVE.getValue());

        when(repository.findById(existingSyllabusId)).thenReturn(Optional.of(existingSyllabus));
        when(repository.save(any(Syllabus.class))).thenReturn(updatedSyllabus);

        // Act
        SyllabusDto result = syllabusService.createAndUpdate(syllabusDto);

        // Assert
        assertNotNull(result);
        assertEquals(existingSyllabusId, result.getId());
        assertEquals(PublicStatus.ACTIVE.getValue(), result.getPublicStatus());

        // Additional assertions based on your logic and expectations
    }

    @Test
    public void testCreateAndUpdate_NewSyllabus() {
        // Prepare test data
        SyllabusDto syllabusDto = new SyllabusDto();
        syllabusDto.setSyllabusName("New Syllabus");

        // Mock repository behavior
        when(repository.save(any(Syllabus.class))).thenReturn(new Syllabus());

        // Perform the method call
        SyllabusDto result = syllabusService.createAndUpdate(syllabusDto);

        // Verify repository save was called
        verify(repository, times(1)).save(any(Syllabus.class));
    }
    @Test
    public void testCreateAndUpdate_SyllabusNotPresent() {
        // Prepare test data
        SyllabusDto syllabusDto = new SyllabusDto();
        syllabusDto.setId(1L);
        syllabusDto.setSyllabusName("New Syllabus");

        // Mock repository behavior
        when(repository.save(any(Syllabus.class))).thenReturn(new Syllabus());
        when(repository.findById(any())).thenReturn(Optional.empty());
        // Perform the method call
        SyllabusDto result = syllabusService.createAndUpdate(syllabusDto);

        // Verify repository save was called
        verify(repository, times(1)).save(any(Syllabus.class));
    }

    @Test
    public void testCreateAndUpdate_UpdateExistingSyllabus() {
        // Prepare test data
        SyllabusDto syllabusDto = new SyllabusDto();
        syllabusDto.setId(1L);
        syllabusDto.setSyllabusName("Updated Syllabus");
        syllabusDto.setPublicStatus(PublicStatus.ACTIVE.name());
        // Mock repository behavior
        Syllabus existingSyllabus = new Syllabus();
        existingSyllabus.setId(1L);
        // Set the publicStatus to null for testing purposes
        existingSyllabus.setPublicStatus(PublicStatus.ACTIVE.name());
        when(repository.findById(1L)).thenReturn(Optional.of(existingSyllabus));
        when(repository.save(existingSyllabus)).thenReturn(existingSyllabus);

        // Perform the method call
        SyllabusDto result = syllabusService.createAndUpdate(syllabusDto);

        // Verify repository save and findById were called
        verify(repository, times(1)).findById(1L);
        verify(repository, times(2)).save(any(Syllabus.class));

        // Add assertions based on your specific logic and expected behavior
        // For example, assert that the generated code is set, etc.

        // Check for null before invoking equals
        if (existingSyllabus.getPublicStatus() != null) {
            assertEquals(existingSyllabus.getPublicStatus(), result.getPublicStatus());
        } else {
            assertNull(result.getPublicStatus()); // Adjust this based on your application logic
        }
    }


    @Test
    public void testGetSyllabusById() {
        // Mock data
        Long syllabusId = 1L;
        Syllabus mockSyllabus = new Syllabus();
        mockSyllabus.setId(syllabusId);

        // Mock repository behavior
        when(repository.findById(syllabusId)).thenReturn(Optional.of(mockSyllabus));
        when(daySyllabusRepository.findBySyllabusId(syllabusId)).thenReturn(Collections.singletonList(new DaySyllabus()));
        when(assignmentShemeRepository.findBySyllabusId(syllabusId)).thenReturn(Collections.singletonList(new AssignmentSheme()));
        when(otherSyllabusRepository.findBySyllabusId(syllabusId)).thenReturn(Collections.singletonList(new OtherSyllabus()));

        // Perform the method call
        SyllabusDetailDto result = syllabusService.getSyllabusById(syllabusId);

        // Verify repository methods were called
        Mockito.verify(repository, Mockito.times(1)).findById(syllabusId);
        Mockito.verify(daySyllabusRepository, Mockito.times(1)).findBySyllabusId(syllabusId);
        Mockito.verify(assignmentShemeRepository, Mockito.times(1)).findBySyllabusId(syllabusId);
        Mockito.verify(otherSyllabusRepository, Mockito.times(1)).findBySyllabusId(syllabusId);

        // Add assertions based on your specific logic and expected behavior
        assertEquals(1, result.getSyllabusDto().size());
    }

    @Test
    public void testGetAllSyllabusAndSearch() {
        // Arrange
        int pageNo = 0;
        int pageSize = 2;
        String keyword = "sample";

        List<Syllabus> syllabusList = new ArrayList<>();
        syllabusList.add(new Syllabus());
        syllabusList.add(new Syllabus());

        Page<Syllabus> syllabusPage = new PageImpl<>(syllabusList);

        when(repository.findAll(any(Specification.class), any(Pageable.class))).thenReturn(syllabusPage);

        // Act
        PagingSyllabusDto result = syllabusService.getAllSyllabusAndSearch(pageNo, pageSize, keyword);

        // Assert
        assertNotNull(result);
        assertEquals(syllabusList.size(), result.getContent().size());
        assertEquals(pageNo, result.getPageNo());
        assertEquals(pageSize, result.getPageSize());
        assertEquals(syllabusList.size(), result.getTotalElements());
        assertEquals(1, result.getTotalPages());
        assertTrue(result.isLast());
        assertEquals(keyword, result.getSearch());

        // Additional assertions based on your logic and expectations
    }

    @Test
    public void testDuplicateSyllabus() {
        // Arrange
        Long syllabusId = 1L;

        // Create original syllabus
        Syllabus originalSyllabus = new Syllabus();
        originalSyllabus.setId(syllabusId);
        originalSyllabus.setSyllabusName("Original Syllabus");

        // Create original training materials
        List<TrainingMaterials> originalTrainingMaterials = new ArrayList<>();
        TrainingMaterials materials = new TrainingMaterials();
        originalTrainingMaterials.add(materials);

        // Create original training contents
        List<TrainingContent> originalTrainingContents = new ArrayList<>();
        TrainingContent content = new TrainingContent();
        content.setTrainingMaterials(originalTrainingMaterials);
        content.setDeliveryType(DeliveryType.Assignment);
        content.setOutputStandard(List.of(OutputStandard.C3SD));
        originalTrainingContents.add(content);

        // Create original training units
        List<TrainingUnit> originalTrainingUnits = new ArrayList<>();
        TrainingUnit unit = new TrainingUnit();
        unit.setTrainingContents(originalTrainingContents);
        originalTrainingUnits.add(unit);


        List<DaySyllabus> originalDaySyllabuses = new ArrayList<>();
        DaySyllabus day = new DaySyllabus();
        day.setTrainingUnit(originalTrainingUnits);
        originalDaySyllabuses.add(day);

        originalSyllabus.setDaSyllabus(originalDaySyllabuses);


        // Mock repository behavior
        when(repository.findById(syllabusId)).thenReturn(Optional.of(originalSyllabus));
        when(repository.existsBySyllabusName(anyString())).thenReturn(false);
        when(repository.save(any(Syllabus.class))).thenReturn(originalSyllabus);

        when(daySyllabusRepository.save(any(DaySyllabus.class))).thenReturn(new DaySyllabus());
        when(trainingUnitRepository.save(any(TrainingUnit.class))).thenReturn(new TrainingUnit());
        when(trainingContentRepository.save(any(TrainingContent.class))).thenReturn(new TrainingContent());
        when(trainingMaterialsRepository.save(any(TrainingMaterials.class))).thenReturn(new TrainingMaterials());

        OtherSyllabus otherSyllabus = new OtherSyllabus();
        otherSyllabus.setId(1L);
        otherSyllabus.setSyllabus(originalSyllabus);
        when(otherSyllabusRepository.findBySyllabusId(syllabusId)).thenReturn(List.of(otherSyllabus));
        when(otherSyllabusRepository.save(any())).thenReturn(any());

        AssignmentSheme assignmentSheme = new AssignmentSheme();
        assignmentSheme.setId(1L);
        assignmentSheme.setSyllabus(originalSyllabus);
        when(assignmentShemeRepository.findBySyllabusId(syllabusId)).thenReturn(List.of(assignmentSheme));
        when(assignmentShemeRepository.save(any())).thenReturn(any());
        // Act
        SyllabusDto duplicatedSyllabusDto = syllabusService.duplicateSyllabus(syllabusId);

        // Assert
        assertNotNull(duplicatedSyllabusDto);
        // Add additional assertions based on your expectations
    }
    @Test
    public void testDuplicateSyllabus_NotFound() {
        // Arrange
        Long syllabusId = 1L;

        // Mock repository behavior
        when(repository.findById(syllabusId)).thenReturn(null);

        assertThrows(SyllabusNotFoundException.class, ()-> syllabusService.duplicateSyllabus(syllabusId));
    }

    @Test
    public void testImportSyllabusFromCSV_ValidData() throws Exception {
        // Mock the MultipartFile
        MockMultipartFile file = new MockMultipartFile("file", "sample.csv", "text/csv", "SyllabusName,Version,AttendeeNumber,Duration,TechnicalRequirement,CourseObjectives,Level\nSyllabus Name Test,V1,100,5,TechnicalRequirement1,CourseObjectives1,Level1".getBytes());

        // Mock repository save method
        when(repository.save(any(Syllabus.class))).thenReturn(new Syllabus());

        // Call the method under test
        syllabusService.importSyllabusFromCSV(file, "createdBy", "modifiedBy", "Replace");

        // Verify that the repository.save method was called
        verify(repository, times(1)).save(any(Syllabus.class));
    }

    @Test
    public void testImportSyllabusFromCSV_InvalidCSV() {
        // Mock the MultipartFile with invalid CSV data
        MockMultipartFile file = new MockMultipartFile("file", "sample.jpg", "text/csv", "invalid,csv,data".getBytes());

        // Call the method under test and assert a general exception
        assertThrows(
                BadRequestException.class,  // Change this to a more general exception
                () -> syllabusService.importSyllabusFromCSV(file, "createdBy", "modifiedBy", "Replace"),
                "Expected an exception, but no exception was thrown"
        );
    }

    @Test
    public void testIsCsvFile_ValidCsvFileName() {
        assertTrue(syllabusService.isCsvFile("sample.csv"));
        assertTrue(syllabusService.isCsvFile("file.CSV"));
    }

    @Test
    public void testIsCsvFile_InvalidCsvFileName() {
        assertFalse(syllabusService.isCsvFile("data.txt"));
        assertFalse(syllabusService.isCsvFile("document.pdf"));
        assertFalse(syllabusService.isCsvFile("spreadsheet.xlsx"));
        assertFalse(syllabusService.isCsvFile("noExtension"));
        assertFalse(syllabusService.isCsvFile(".hidden"));
    }

    @Test
    public void testIsCsvFile_NullFileName() {
        assertFalse(syllabusService.isCsvFile(null));
    }

    @Test
    public void testIsCsvFile_EmptyFileName() {
        assertFalse(syllabusService.isCsvFile(""));
    }

    @Test
    public void testGetSyllabusEntityById_ExistingId() {
        // Arrange
        Long syllabusId = 1L;
        Syllabus expectedSyllabus = new Syllabus();
        when(repository.findById(syllabusId)).thenReturn(Optional.of(expectedSyllabus));

        // Act
        Syllabus result = syllabusService.getSyllabusEntityById(syllabusId);

        // Assert
        assertEquals(expectedSyllabus, result);
        verify(repository, times(1)).findById(syllabusId);
    }

    @Test
    public void testGetSyllabusEntityById_NonExistingId() {
        // Arrange
        Long syllabusId = 2L;
        when(repository.findById(syllabusId)).thenReturn(Optional.empty());

        // Act
        Syllabus result = syllabusService.getSyllabusEntityById(syllabusId);

        // Assert
        assertNull(result);
        verify(repository, times(1)).findById(syllabusId);
    }

    @Test
    public void testDeleteSyllabusById_ExistingId() throws SyllabusNotFoundException {
        // Arrange
        Long syllabusId = 1L;
        Syllabus syllabus = new Syllabus();
        syllabus.setId(syllabusId);

        when(repository.findById(syllabusId)).thenReturn(Optional.of(syllabus));
        when(daySyllabusRepository.findBySyllabusId(syllabusId)).thenReturn(Collections.singletonList(new DaySyllabus()));
        when(trainingUnitRepository.findTrainingUnitByDaySyllabus(any(DaySyllabus.class))).thenReturn(new TrainingUnit());
        when(trainingContentRepository.findTrainingContentByTrainingUnit(any(TrainingUnit.class))).thenReturn(new TrainingContent());
        when(trainingMaterialsRepository.findTrainingMaterialsByTrainingContent(any(TrainingContent.class))).thenReturn(new TrainingMaterials());
        doNothing().when(s3Service).deleteObject(anyString(), anyString());

        // Act
        syllabusService.deleteSyllabusById(syllabusId);

        // Assert
        verify(trainingMaterialsRepository, times(1)).findTrainingMaterialsByTrainingContent(any(TrainingContent.class));
        verify(s3Service, times(0)).deleteObject(anyString(), anyString());
        verify(repository, times(1)).delete(syllabus);
    }

    @Test
    public void testDeleteSyllabusById_NonExistingId() {
        // Arrange
        Long syllabusId = 2L;
        when(repository.findById(syllabusId)).thenReturn(Optional.empty());

        // Act and Assert
        assertThrows(SyllabusNotFoundException.class, () -> syllabusService.deleteSyllabusById(syllabusId));
    }

    @Test
    public void testDeleteSyllabus_Success(){
            // Arrange
            Long syllabusId = 1L;

            // Create original training materials
            List<TrainingMaterials> originalTrainingMaterials = new ArrayList<>();
            TrainingMaterials materials = new TrainingMaterials();
            originalTrainingMaterials.add(materials);

            // Create original training contents
            List<TrainingContent> originalTrainingContents = new ArrayList<>();
            TrainingContent content = new TrainingContent();
            content.setTrainingMaterials(originalTrainingMaterials);
            originalTrainingContents.add(content);

            // Create original training units
            List<TrainingUnit> originalTrainingUnits = new ArrayList<>();
            TrainingUnit unit = new TrainingUnit();
            unit.setTrainingContents(originalTrainingContents);
            originalTrainingUnits.add(unit);


            List<DaySyllabus> originalDaySyllabuses = new ArrayList<>();
            DaySyllabus day = new DaySyllabus();
            day.setTrainingUnit(originalTrainingUnits);
            originalDaySyllabuses.add(day);

        Syllabus syllabus = new Syllabus();
        syllabus.setId(syllabusId);
        syllabus.setPublicStatus(PublicStatus.DRAFT.name());

            syllabus.setDaSyllabus(originalDaySyllabuses);



        // Mock repository methods
        when(repository.findById(syllabusId)).thenReturn(Optional.of(syllabus));

        // Act
        syllabusService.deleteSyllabus(syllabusId);

        // Assert and Verify
        verify(s3Service, times(0)).deleteObject(anyString(), anyString());
        verify(trainingMaterialsRepository, times(1)).deleteAll(anyList());
        verify(trainingContentRepository, times(1)).deleteAll(anyList());
        verify(trainingUnitRepository, times(1)).deleteAll(anyList());
        verify(daySyllabusRepository, times(1)).deleteAll(anyList());
        verify(repository, times(1)).delete(syllabus);
    }

    @Test
    public void testDeleteSyllabus_SyllabusNotFound() {
        // Arrange
        Long syllabusId = 1L;
        when(repository.findById(syllabusId)).thenReturn(Optional.empty());

        // Act and Assert
        SyllabusNotFoundException exception = assertThrows(SyllabusNotFoundException.class, () -> syllabusService.deleteSyllabus(syllabusId));

        // Assert specific details if needed
        assertTrue(exception.getMessage().contains("Syllabus not found"));
    }
    @Test
    public void testDeleteSyllabus_SyllabusInUse() {
        // Arrange
        Long syllabusId = 1L;
        Syllabus syllabus = new Syllabus();
        syllabus.setId(syllabusId);
        syllabus.setPublicStatus(PublicStatus.ACTIVE.name());
        when(repository.findById(syllabusId)).thenReturn(Optional.of(syllabus));

        // Act and Assert
        BadRequestException exception = assertThrows(BadRequestException.class, () -> syllabusService.deleteSyllabus(syllabusId));

        // Assert specific details if needed
        assertTrue(exception.getMessage().contains("The syllabus is in use"));
    }

    @Test
    void testGetListSyllabusForService() {
        // Arrange
        List<RequestIdDto> requestList = new ArrayList<>();
        requestList.add(new RequestIdDto(1L));
        requestList.add(new RequestIdDto(2L));

        Syllabus syllabus1 = new Syllabus();
        syllabus1.setId(1L);
        when(repository.findById(1L)).thenReturn(Optional.of(syllabus1));

        Syllabus syllabus2 = new Syllabus();
        syllabus2.setId(2L);
        when(repository.findById(2L)).thenReturn(Optional.of(syllabus2));

        // Act
        List<SyllabusDto> result = syllabusService.getListSyllabusFOrService(requestList);

        // Assert
        assertEquals(2, result.size());

        // Verify that the expected methods were called
        verify(repository, times(1)).findById(1L);
        verify(repository, times(1)).findById(2L);
    }

    @Test
    void testGetAllSyllabus() {
        // Arrange
        Syllabus syllabus1 = new Syllabus();
        syllabus1.setId(1L);
        Syllabus syllabus2 = new Syllabus();
        syllabus2.setId(2L);

        List<Syllabus> syllabusList = Arrays.asList(syllabus1, syllabus2);
        when(repository.findAll()).thenReturn(syllabusList);

        // Act
        List<SyllabusDto> result = syllabusService.getAllSyllabus();

        // Assert
        assertEquals(2, result.size());
        assertEquals(1L, result.get(0).getId());
        assertEquals(2L, result.get(1).getId());

        // Verify that the expected methods were called
        verify(repository, times(1)).findAll();
    }

//    @Test
//    void searchSyllabus() {
//        // Arrange
//        String keyword = "test";
//        when(criteriaBuilder.or(any())).thenAnswer(invocation -> {
//            Predicate[] predicates = invocation.getArgument(0);
//            return criteriaBuilder.or(predicates);  // This assumes you are using criteriaBuilder.or(Predicate...)
//        });
//        when(criteriaBuilder.and(any())).thenAnswer(invocation -> {
//            Predicate[] predicates = invocation.getArgument(0);
//            return criteriaBuilder.and(predicates); // This assumes you are using criteriaBuilder.and(Predicate...)
//        });
//
//        // Act
//        syllabusService.searchSyllabus(keyword).toPredicate(root, criteriaQuery, criteriaBuilder);
//
//        // Assert
//        // You can add assertions based on the expected behavior of the method
//        verify(criteriaBuilder, times(1)).or(any());
//        verify(criteriaBuilder, times(1)).and(any());
//        // Add more verifications as needed
//    }


    @Test
    void testGetSyllabusEntityByIdWhenSyllabusExists() {
        // Arrange
        Long syllabusId = 1L;
        Syllabus syllabus = new Syllabus();
        when(repository.findById(syllabusId)).thenReturn(Optional.of(syllabus));

        // Act
        Syllabus result = syllabusService.getSyllabusEntityById(syllabusId);

        // Assert
        assertEquals(syllabus, result);
    }

    @Test
    void testGetSyllabusEntityByIdWhenSyllabusNotExists() {
        // Arrange
        Long syllabusId = 1L;
        when(repository.findById(syllabusId)).thenReturn(Optional.empty());

        // Act
        Syllabus result = syllabusService.getSyllabusEntityById(syllabusId);

        assertNull(result);
    }

    @Test
    void testDeleteSyllabusByIdWhenSyllabusExists() {
        // Arrange
        Long syllabusId = 1L;
        Syllabus syllabusToDelete = new Syllabus();
        when(repository.findById(syllabusId)).thenReturn(Optional.of(syllabusToDelete));

        // Act
        syllabusService.deleteSyllabusById(syllabusId);

        // Assert
        verify(repository).delete(syllabusToDelete);
    }

    @Test
    void testDeleteSyllabusByIdWhenSyllabusNotExists() {
        // Arrange
        Long syllabusId = 1L;
        when(repository.findById(syllabusId)).thenReturn(Optional.empty());

        // Act and Assert
        try {
            syllabusService.deleteSyllabusById(syllabusId);
        } catch (SyllabusNotFoundException exception) {
            assertEquals("Syllabus Not Found", exception.getMessage());
        }
    }

    @Test
    public void testSearchSyllabus_WithKeyword() {
        // Arrange
        String keyword = "searchKeyword";
        Root<Syllabus> root = mock(Root.class);
        CriteriaQuery<?> query = mock(CriteriaQuery.class);
        CriteriaBuilder criteriaBuilder = mock(CriteriaBuilder.class);

        // Act
        Specification<Syllabus> spec = syllabusService.searchSyllabus(keyword);
        Predicate predicate = spec.toPredicate(root, query, criteriaBuilder);

        // Assert
        Predicate expectedPredicate = criteriaBuilder.and(
                criteriaBuilder.or(
                        criteriaBuilder.like(root.get("syllabusName"), "%searchKeyword%"),
                        criteriaBuilder.like(root.get("syllabusCode"), "%searchKeyword%")
                )
        );
        assertEquals(expectedPredicate, predicate);
    }

    @Test
    public void testSearchSyllabus_WithoutKeyword() {
        String keyword = null;
        Root<Syllabus> root = Mockito.mock(Root.class);
        CriteriaQuery<?> query = Mockito.mock(CriteriaQuery.class);
        CriteriaBuilder criteriaBuilder = Mockito.mock(CriteriaBuilder.class);
        List<Predicate> predicates = new ArrayList<>();

        Specification<Syllabus> specification = syllabusService.searchSyllabus(keyword);
        Predicate resultPredicate = specification.toPredicate(root, query, criteriaBuilder);

        Mockito.verify(criteriaBuilder, Mockito.never()).like(Mockito.any(), Mockito.anyString());
        Mockito.verify(criteriaBuilder).and(predicates.toArray(new Predicate[0]));

    }
}
