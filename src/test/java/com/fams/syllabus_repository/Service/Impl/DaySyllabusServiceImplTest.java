package com.fams.syllabus_repository.Service.Impl;

import com.fams.syllabus_repository.converter.DaySyllabusConverter;
import com.fams.syllabus_repository.converter.TrainingContentConverter;
import com.fams.syllabus_repository.converter.TrainingUnitConverter;
import com.fams.syllabus_repository.dto.*;
import com.fams.syllabus_repository.entity.DaySyllabus;
import com.fams.syllabus_repository.entity.Syllabus;
import com.fams.syllabus_repository.entity.TrainingContent;
import com.fams.syllabus_repository.entity.TrainingUnit;
import com.fams.syllabus_repository.enums.DeliveryType;
import com.fams.syllabus_repository.exceptions.DaySyllabusNotFoundException;
import com.fams.syllabus_repository.exceptions.SyllabusNotFoundException;
import com.fams.syllabus_repository.repository.DaySyllabusRepository;
import com.fams.syllabus_repository.repository.SyllabusRepository;
import com.fams.syllabus_repository.repository.TrainingContentRepository;
import com.fams.syllabus_repository.repository.TrainingUnitRepository;
import com.fams.syllabus_repository.service.impl.DaySyllabusServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;

import java.time.LocalDate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class DaySyllabusServiceImplTest {
    @InjectMocks
    private DaySyllabusServiceImpl daySyllabusService;
    @Mock
    private DaySyllabusRepository daySyllabusRepository;
    @Mock
    private TrainingUnitConverter trainingUnitConverter;
    @Mock
    private SyllabusRepository syllabusRepository;
    @Mock
    private TrainingContentConverter trainingContentConverter;

    @Mock
    private TrainingUnitRepository trainingUnitRepository;
    @Mock
    private TrainingContentRepository trainingContentRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetDaySyllabusBySyllabusId() {
        Long syllabusId = 1L;

        // Create a sample DaySyllabus and its associated TrainingUnits
        DaySyllabus mockDaySyllabus = new DaySyllabus();
        mockDaySyllabus.setId(1L);
        mockDaySyllabus.setDay("Day 1");

        TrainingUnit trainingUnit1 = new TrainingUnit();
        trainingUnit1.setId(101L);
        trainingUnit1.setUnitName("Unit 1");

        TrainingUnit trainingUnit2 = new TrainingUnit();
        trainingUnit2.setId(102L);
        trainingUnit2.setUnitName("Unit 2");

        mockDaySyllabus.setTrainingUnit(Arrays.asList(trainingUnit1, trainingUnit2));

        // Mock the behavior of daySyllabusRepository
        when(daySyllabusRepository.findBySyllabusId(syllabusId))
                .thenReturn(Collections.singletonList(mockDaySyllabus));

        // Call the method under test
        List<DaySyllabusListDto> result = daySyllabusService.getDaySyllabusBySyllabusId(syllabusId);

        // Assertions
        assertEquals(1, result.size()); // Ensure there is one DaySyllabus in the result

        DaySyllabusListDto daySyllabusDto = result.get(0);
        assertEquals(mockDaySyllabus.getId(), daySyllabusDto.getId()); // Verify the ID
        assertEquals(mockDaySyllabus.getDay(), daySyllabusDto.getDay()); // Verify the name

        // Verify the TrainingUnitListDtos size
        assertEquals(2, daySyllabusDto.getTrainingUnitListDtos().size());

        // You can add more specific assertions for TrainingUnitListDtos if needed
    }
    @Test
    public void testCreateDaySyllabus() {
        // Mock input data
        Long syllabusId = 1L;
        DaySyllabusDto daySyllabusDto = new DaySyllabusDto();
        daySyllabusDto.setDay("Monday");

        // Mock behavior của repository
        Syllabus syllabus = new Syllabus();
        syllabus.setId(syllabusId);
        when(syllabusRepository.findById(syllabusId)).thenReturn(Optional.of(syllabus));

        DaySyllabus savedDaySyllabus = new DaySyllabus();
        savedDaySyllabus.setDay(daySyllabusDto.getDay());
        when(daySyllabusRepository.save(any())).thenReturn(savedDaySyllabus);

        // Gọi phương thức cần kiểm tra
        DaySyllabusDto result = daySyllabusService.createDaySyllabus(syllabusId, daySyllabusDto);

        // Kiểm tra kết quả
        assertNotNull(result);
        assertEquals(daySyllabusDto.getDay(), result.getDay());
    }
    @Test
    public void testDeleteDaySyllabus() {
        // Arrange
        Long syllabusId = 1L;
        Long dayId = 2L;

        Syllabus syllabus = new Syllabus(); // Initialize with necessary properties
        when(syllabusRepository.findById(syllabusId)).thenReturn(Optional.of(syllabus));

        DaySyllabus daySyllabus = new DaySyllabus(); // Initialize with necessary properties
        when(daySyllabusRepository.findById(dayId)).thenReturn(Optional.of(daySyllabus));

        // Act
        daySyllabusService.deleteDaySyllabus(syllabusId, dayId);

        // Assert
        verify(syllabusRepository, times(1)).findById(syllabusId);
        verify(daySyllabusRepository, times(1)).findById(dayId);
        verify(daySyllabusRepository, times(1)).delete(daySyllabus);
    }

    @Test
    public void testDeleteDaySyllabus_SyllabusNotFound() {
        // Arrange
        Long syllabusId = 1L;
        Long dayId = 2L;

        when(syllabusRepository.findById(syllabusId)).thenReturn(Optional.empty());

        // Act and Assert
        assertThrows(SyllabusNotFoundException.class, () -> daySyllabusService.deleteDaySyllabus(syllabusId, dayId));

        // Verify that the daySyllabusRepository methods are not called
        verify(syllabusRepository, times(1)).findById(syllabusId);
        verify(daySyllabusRepository, never()).findById(anyLong());
        verify(daySyllabusRepository, never()).delete(any());
    }

    @Test
    public void testDeleteDaySyllabus_DaySyllabusNotFound() {
        // Arrange
        Long syllabusId = 1L;
        Long dayId = 2L;

        Syllabus syllabus = new Syllabus(); // Initialize with necessary properties
        when(syllabusRepository.findById(syllabusId)).thenReturn(Optional.of(syllabus));
        when(daySyllabusRepository.findById(dayId)).thenReturn(Optional.empty());

        // Act and Assert
        assertThrows(DaySyllabusNotFoundException.class, () -> daySyllabusService.deleteDaySyllabus(syllabusId, dayId));

        // Verify that the daySyllabusRepository methods are called
        verify(syllabusRepository, times(1)).findById(syllabusId);
        verify(daySyllabusRepository, times(1)).findById(dayId);
        verify(daySyllabusRepository, never()).delete(any());
    }

    @Test
    public void testDeleteDaySyllabusById() {
        // Arrange
        Long dayId = 1L;
        DaySyllabus daySyllabus = new DaySyllabus();
        daySyllabus.setId(dayId);

        // Mocking repository behavior
        when(daySyllabusRepository.findById(dayId)).thenReturn(Optional.of(daySyllabus));

        // Act
        daySyllabusService.deleteDaySyllabusById(dayId);

        // Assert
        verify(daySyllabusRepository, Mockito.times(1)).delete(daySyllabus);
    }

    @Test
    public void testDeleteDaySyllabusByIdNotFound() {
        // Arrange
        Long dayId = 1L;

        // Mocking repository behavior for a non-existing DaySyllabus
        when(daySyllabusRepository.findById(dayId)).thenReturn(Optional.empty());

        // Act and Assert
        assertThrows(DaySyllabusNotFoundException.class, () -> daySyllabusService.deleteDaySyllabusById(dayId));
    }

    @Test
    public void testUpdateDaySyllabusList() {
        // Arrange
        Long syllabusId = 1L;
        List<DaySyllabusDto> daySyllabusDtos = new ArrayList<>();

        // Add DaySyllabusDto instances to daySyllabusDtos as needed

        // Mock repository methods
        when(syllabusRepository.findById(syllabusId)).thenReturn(java.util.Optional.of(new Syllabus()));

        // Mock daySyllabusRepository.save method
        when(daySyllabusRepository.save(any(DaySyllabus.class))).thenAnswer(invocation -> {
            DaySyllabus daySyllabus = invocation.getArgument(0);
            daySyllabus.setId(1L);  // Simulating the save operation
            return daySyllabus;
        });

        // Mock trainingUnitRepository.save method
        when(trainingUnitRepository.save(any(TrainingUnit.class))).thenAnswer(invocation -> {
            TrainingUnit trainingUnit = invocation.getArgument(0);
            trainingUnit.setId(1L);  // Simulating the save operation
            return trainingUnit;
        });

        // Mock trainingContentRepository.save method
        when(trainingContentRepository.save(any(TrainingContent.class))).thenAnswer(invocation -> {
            TrainingContent trainingContent = invocation.getArgument(0);
            trainingContent.setId(1L);  // Simulating the save operation
            return trainingContent;
        });

        // Act
        List<DaySyllabusDto> result = daySyllabusService.updateDaySyllabusList(syllabusId, daySyllabusDtos);

        // Assert
        assertNotNull(result);
        // Add more assertions based on your specific implementation and needs
    }

    @Test
    public void testUpdateDaySyllabusList_SyllabusNotFound() {
        // Arrange
        Long syllabusId = 1L;
        List<DaySyllabusDto> daySyllabusDtos = new ArrayList<>();
        DaySyllabusDto daySyllabus = new DaySyllabusDto();
        daySyllabus.setId(1L);
        daySyllabusDtos.add(daySyllabus);

        // Mock SyllabusRepository to return an empty Optional
        when(daySyllabusRepository.findById(1L)).thenReturn(Optional.empty());

        // Act & Assert
        DaySyllabusNotFoundException exception = assertThrows(DaySyllabusNotFoundException.class, () -> {
            daySyllabusService.updateDaySyllabusList(syllabusId, daySyllabusDtos);
        });

        // You can add additional assertions if needed
        assertTrue(exception.getMessage().contains("Syllabus not found"));
    }

    @Test
    public void testCreateDaySyllabusList() {
        // Arrange
        Long syllabusId = 1L;

        // Mock Syllabus
        Syllabus syllabus = new Syllabus();
        syllabus.setId(syllabusId);
        when(syllabusRepository.findById(syllabusId)).thenReturn(Optional.of(syllabus));

        // Mock TrainingUnit save operation
        when(trainingUnitRepository.save(any(TrainingUnit.class))).thenAnswer(invocation -> {
            TrainingUnit savedUnit = invocation.getArgument(0);
            savedUnit.setId(1L); // Simulating the save operation
            return savedUnit;
        });

        // Mock DaySyllabus save operation
        when(daySyllabusRepository.save(any(DaySyllabus.class))).thenAnswer(invocation -> {
            DaySyllabus savedDaySyllabus = invocation.getArgument(0);
            savedDaySyllabus.setId(1L); // Simulating the save operation
            return savedDaySyllabus;
        });

        // Create DaySyllabusDto list
        List<DaySyllabusDto> daySyllabusDtos = new ArrayList<>();
        // Add DaySyllabusDto instances to daySyllabusDtos as needed

        // Act
        List<DaySyllabusDto> result = daySyllabusService.createDaySyllabusList(syllabusId, daySyllabusDtos);

        // Assert
        assertNotNull(result);
        // Add more assertions based on your specific implementation and needs
    }

    @Test
    public void testCreateDaySyllabusList_SyllabusNotFound() {
        // Arrange
        Long syllabusId = 1L;

        // Mock Syllabus not found
        when(syllabusRepository.findById(syllabusId)).thenReturn(Optional.empty());

        // Create DaySyllabusDto list
        List<DaySyllabusDto> daySyllabusDtos = new ArrayList<>();
        // Add DaySyllabusDto instances to daySyllabusDtos as needed

        // Act and Assert
        DaySyllabusNotFoundException exception = assertThrows(DaySyllabusNotFoundException.class, () ->
                daySyllabusService.createDaySyllabusList(syllabusId, daySyllabusDtos)
        );

        assertEquals("Syllabus not found", exception.getMessage());
    }

    @Test
    void testTrainingUnitListToDtoList() {
        // Arrange
        List<TrainingUnit> trainingUnits = new ArrayList<>();
        TrainingUnit unit1 = new TrainingUnit();
        unit1.setUnitName("Unit 1");
        unit1.setUnitNumber("001");

        TrainingUnit unit2 = new TrainingUnit();
        unit2.setUnitName("Unit 2");
        unit2.setUnitNumber("002");

        trainingUnits.add(unit1);
        trainingUnits.add(unit2);

        // Mock the behavior if necessary (e.g., if there's a dependency in the method)
        // when(trainingUnitRepository.someMethod()).thenReturn(someValue);

        // Act
        List<TrainingUnitListDto> result = daySyllabusService.trainingUnitListToDtoList(trainingUnits);

        // Assert
        assertEquals(trainingUnits.size(), result.size());

        for (int i = 0; i < trainingUnits.size(); i++) {
            TrainingUnitListDto dto = result.get(i);
            TrainingUnit unit = trainingUnits.get(i);

            // Add more assertions based on your needs
            assertEquals(unit.getUnitName(), dto.getUnitName());
            assertEquals(unit.getUnitNumber(), dto.getUnitNumber());
            assertEquals(LocalDate.now(), dto.getCreatedDate());
            assertEquals(LocalDate.now(), dto.getModifyDate());
        }
    }

    @Test
    public void testToEntityDaySyllabusDto() {
        // Tạo một đối tượng DaySyllabusDto
        DaySyllabusDto daySyllabusDto = new DaySyllabusDto();
        daySyllabusDto.setDay("Monday");

        // Gọi hàm chuyển đổi
        DaySyllabus daySyllabus = DaySyllabusServiceImpl.toEntity(daySyllabusDto);

        // Kiểm tra kết quả
        assertNotNull(daySyllabus);
        assertNull(daySyllabus.getId()); // Vì id không được đặt trong hàm toEntity
        assertEquals("Monday", daySyllabus.getDay());
        assertNotNull(daySyllabus.getCreatedDate());
        assertNotNull(daySyllabus.getModifyDate());
    }

    @Test
    public void testToEntityTrainingUnitListDto() {
        // Arrange
        TrainingContent trainingContent = new TrainingContent();
        trainingContent.setId(1L);
        trainingContent.setName("Sample Training Content");
        trainingContent.setDeliveryType(DeliveryType.Exam);
        trainingContent.setTrainingTime(60);
        trainingContent.setOutputStandard(new ArrayList<>()); // Assuming OutputStandard is an enum
        trainingContent.setMethod("Sample Method");
        trainingContent.setCreatedDate(LocalDate.now());
        trainingContent.setCreatedBy("user1");
        trainingContent.setModifyDate(LocalDate.now());
        trainingContent.setModifyBy("user2");

        // Act
        TrainingContentDto resultDto = TrainingContentConverter.toDto(trainingContent);

        // Assert
        assertEquals(trainingContent.getId(), resultDto.getId());
        assertEquals(trainingContent.getName(), resultDto.getName());
        assertEquals(trainingContent.getDeliveryType().toString(), resultDto.getDeliveryType());
        assertEquals(trainingContent.getTrainingTime(), resultDto.getTrainingTime());
        assertEquals(trainingContent.getOutputStandard(), resultDto.getOutputStandard());
        assertEquals(trainingContent.getMethod(), resultDto.getMethod());
        assertEquals(trainingContent.getCreatedDate(), resultDto.getCreatedDate());
        assertEquals(trainingContent.getCreatedBy(), resultDto.getCreatedBy());
        assertEquals(trainingContent.getModifyDate(), resultDto.getModifyDate());

        // Check for null directly
        assertNull(resultDto.getModifyBy());

    }

    @Test
    public void testToEntityUnitList() {
        // Arrange
        MockitoAnnotations.initMocks(this);
        TrainingUnitListDto dto = new TrainingUnitListDto();
        dto.setUnitName("Sample Unit");
        dto.setUnitNumber("001");
        dto.setCreatedBy("user1");
        dto.setModifyBy("user1");

        // Create a TrainingUnitDto object
        TrainingUnitDto trainingUnitDto = new TrainingUnitDto();
        trainingUnitDto.setUnitName(dto.getUnitName());
        trainingUnitDto.setUnitNumber(dto.getUnitNumber());
        trainingUnitDto.setCreatedBy(dto.getCreatedBy());
        trainingUnitDto.setModifyBy(dto.getModifyBy());

        // Act
        TrainingUnit result = trainingUnitConverter.toEntity(trainingUnitDto);

        // Assert
        assertEquals(trainingUnitDto.getUnitName(), result.getUnitName());
        assertEquals(trainingUnitDto.getUnitNumber(), result.getUnitNumber());
        assertEquals(LocalDate.now(), result.getCreatedDate());
        assertEquals(trainingUnitDto.getCreatedBy(), result.getCreatedBy());
        assertEquals(LocalDate.now(), result.getModifyDate());
        assertEquals(trainingUnitDto.getModifyBy(), result.getModifyBy());

        // Add more assertions based on your specific implementation and needs
    }

    @Test
    public void testToEntity() {
        // Arrange
        TrainingUnitDto dto = new TrainingUnitDto();
        dto.setUnitName("Sample Unit");
        dto.setUnitNumber("001");
        dto.setCreatedBy("user1");
        dto.setModifyBy("user1");

        // Act
        TrainingUnit result = TrainingUnitConverter.toEntity(dto);

        // Assert
        assertEquals(dto.getUnitName(), result.getUnitName());
        assertEquals(dto.getUnitNumber(), result.getUnitNumber());
        assertEquals(LocalDate.now(), result.getCreatedDate());
        assertEquals("user1", result.getCreatedBy());
        assertEquals(LocalDate.now(), result.getModifyDate());
        assertEquals("user1", result.getModifyBy());

        // Add more assertions based on your specific implementation and needs
    }

    @Test
    public void testToEntityUnitLists() {
        // Arrange
        TrainingUnitListDto listDto = new TrainingUnitListDto();
        listDto.setUnitName("Sample Unit");
        listDto.setUnitNumber("001");
        listDto.setCreatedBy("user1");
        listDto.setModifyBy("user1");

        TrainingUnit entity = new TrainingUnit();

        // Act
        TrainingUnit result = daySyllabusService.toEntityUnitList(listDto, entity);

        // Assert
        assertEquals(listDto.getUnitName(), result.getUnitName());
        assertEquals(listDto.getUnitNumber(), result.getUnitNumber());
        assertEquals(LocalDate.now(), result.getCreatedDate());
        assertEquals("user1", result.getCreatedBy());
        assertEquals(LocalDate.now(), result.getModifyDate());
        assertEquals("user1", result.getModifyBy());

        // Add more assertions based on your specific implementation and needs
    }

    @Test
    public void testUnitNameGetterSetter() {
        // Arrange
        TrainingUnit trainingUnit = new TrainingUnit();
        String expectedUnitName = "Unit 1";

        // Act
        trainingUnit.setUnitName(expectedUnitName);
        String actualUnitName = trainingUnit.getUnitName();

        // Assert
        assertEquals(expectedUnitName, actualUnitName);
    }

    @Test
    public void testUnitNumberGetterSetter() {
        // Arrange
        TrainingUnit trainingUnit = new TrainingUnit();
        String expectedUnitNumber = "001";

        // Act
        trainingUnit.setUnitNumber(expectedUnitNumber);
        String actualUnitNumber = trainingUnit.getUnitNumber();

        // Assert
        assertEquals(expectedUnitNumber, actualUnitNumber);
    }

    @Test
    void testToEntityy() {
        // Arrange
        DaySyllabusDto daySyllabusDto = new DaySyllabusDto();
        daySyllabusDto.setDay("Monday");

        // Act
        DaySyllabus result = daySyllabusService.toEntity(daySyllabusDto);

        // Assert
        assertEquals(daySyllabusDto.getDay(), result.getDay());
        assertEquals(LocalDate.now(), result.getCreatedDate()); // Adjust this based on your specific requirements
        assertEquals(LocalDate.now(), result.getModifyDate());  // Adjust this based on your specific requirements
    }
}
