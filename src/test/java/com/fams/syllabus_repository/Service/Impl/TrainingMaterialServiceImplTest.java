package com.fams.syllabus_repository.Service.Impl;

import com.fams.syllabus_repository.converter.TrainingMaterialsConverter;
import com.fams.syllabus_repository.dto.TrainingMaterialsDto;
import com.fams.syllabus_repository.entity.TrainingContent;
import com.fams.syllabus_repository.entity.TrainingMaterials;
import com.fams.syllabus_repository.exceptions.MaterialNotFoundException;
import com.fams.syllabus_repository.exceptions.ResourceNotFoundException;
import com.fams.syllabus_repository.exceptions.SyllabusNotFoundException;
import com.fams.syllabus_repository.repository.TrainingContentRepository;
import com.fams.syllabus_repository.repository.TrainingMaterialsRepositoty;
import com.fams.syllabus_repository.s3.S3Bucket;
import com.fams.syllabus_repository.s3.S3Sevice;
import com.fams.syllabus_repository.service.TrainingMaterialsService;
import com.fams.syllabus_repository.service.impl.TrainingMaterialsServiceImpl;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.junit.Assert;
import org.junit.Before;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.*;

public class TrainingMaterialServiceImplTest {
    @Mock
    private TrainingMaterialsRepositoty trainingMaterialsRepositoty;
    @Mock
    private TrainingContentRepository trainingContentRepository;
    @InjectMocks
    private TrainingMaterialsServiceImpl trainingMaterialsService;
    @Mock
    private S3Bucket s3Bucket;
    @Mock
    private S3Sevice s3Sevice;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetAllResource() {
        // Arrange
        TrainingMaterials trainingMaterials1 = new TrainingMaterials();
        trainingMaterials1.setTitle("Title 1");
        trainingMaterials1.setS3Location("s3://location1");
//        trainingMaterials1.setData(new byte[]{1, 2, 3});

        TrainingMaterials trainingMaterials2 = new TrainingMaterials();
        trainingMaterials2.setTitle("Title 2");
        trainingMaterials2.setS3Location("s3://location2");
//        trainingMaterials2.setData(new byte[]{4, 5, 6});

        List<TrainingMaterials> trainingMaterialsList = Arrays.asList(trainingMaterials1, trainingMaterials2);

        Mockito.when(trainingMaterialsRepositoty.findAll()).thenReturn(trainingMaterialsList);

        // Act
        List<TrainingMaterialsDto> result = trainingMaterialsService.getAllResource();

        // Assert
        Assert.assertEquals(2, result.size());

        TrainingMaterialsDto dto1 = result.get(0);
        Assert.assertEquals(trainingMaterials1.getTitle(), dto1.getTitle());
        Assert.assertEquals(trainingMaterials1.getS3Location(), dto1.getS3Location());
        Assert.assertArrayEquals(trainingMaterials1.getData(), dto1.getTrainingMaterialData());

        TrainingMaterialsDto dto2 = result.get(1);
        Assert.assertEquals(trainingMaterials2.getTitle(), dto2.getTitle());
        Assert.assertEquals(trainingMaterials2.getS3Location(), dto2.getS3Location());
        Assert.assertArrayEquals(trainingMaterials2.getData(), dto2.getTrainingMaterialData());
    }

    @Test
    public void testUploadTrainingMaterial_InvalidFileExtension() throws IOException {
        // Mock input data
        Long trainingContentId = 1L;
        String description = "Sample description";
        String originalFilename = "sample.txt";  // Using an invalid file extension
        byte[] fileData = "Sample file data".getBytes();

        // Mock TrainingContent
        TrainingContent trainingContent = new TrainingContent();
        trainingContent.setId(trainingContentId);
        trainingContent.setName(description);

        // Mock MultipartFile
        MockMultipartFile mockFile = new MockMultipartFile("file", originalFilename, "text/plain", fileData);

        // Mock the behavior of trainingContentRepository
        when(trainingContentRepository.findById(trainingContentId)).thenReturn(Optional.of(trainingContent));
    }

    @Test
    public void testDownloadTrainingMaterial_ExistingMaterial() {
        // Mock materialsId và dữ liệu tài liệu đào tạo (chúng ta giả định rằng có tài liệu đào tạo với materialsId = 1)
        Long materialsId = 1L;
        byte[] expectedData = "Sample file data".getBytes();

        // Mock behavior của repository
        when(trainingMaterialsRepositoty.findById(materialsId))
                .thenReturn(Optional.of(new TrainingMaterials()));

        // Gọi phương thức cần kiểm tra
        byte[] result = trainingMaterialsService.downloadTrainingMaterial(materialsId);

        // Kiểm tra kết quả và so sánh dữ liệu trả về với dữ liệu mong đợi
        // assertArrayEquals(expectedData, result);
    }

    @Test
    public void testDownloadTrainingMaterial_NonExistentMaterial() {
        // Mock materialsId và không tìm thấy tài liệu đào tạo (chúng ta giả định rằng không có tài liệu đào tạo với materialsId = 2)
        Long materialsId = 2L;

        // Mock behavior của repository
        when(trainingMaterialsRepositoty.findById(materialsId)).thenReturn(Optional.empty());

        // Gọi phương thức cần kiểm tra
        byte[] result = trainingMaterialsService.downloadTrainingMaterial(materialsId);

        // Kiểm tra kết quả và xác nhận rằng kết quả là null
        assertNull(result);
    }

    @Test
    public void testUploadTrainingMaterialToS3_TrainingContentNotFound() {
        // Arrange
        Long trainingContentId = 1L;
        MockMultipartFile file = new MockMultipartFile("file", "filename.txt", "text/plain", "file content".getBytes());

        when(trainingContentRepository.findById(trainingContentId)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(RuntimeException.class, () ->
                trainingMaterialsService.uploadTrainingMaterialToS3(trainingContentId, file, "user1", "user2"));

        // Verify interactions
        verify(trainingContentRepository, times(1)).findById(trainingContentId);
        // Ensure that save method is not called on the repository
        verify(trainingMaterialsRepositoty, never()).save(any(TrainingMaterials.class));
    }

//    @Test
//    public void testGetTrainingMaterialDataById_WhenTrainingMaterialExists() throws ResourceNotFoundException {
//        // Arrange
//        Long trainingMaterialId = 1L;
//        String s3Location = "s3://example-bucket/training-materials/file.pdf";
//        byte[] expectedData = new byte[]{1, 2, 3};
//
//        TrainingMaterials trainingMaterial = new TrainingMaterials();
//        trainingMaterial.setId(trainingMaterialId);
//        trainingMaterial.setS3Location(s3Location);
//        S3Bucket bu = mock(S3Bucket.class);
//        Mockito.when(trainingMaterialsRepositoty.findById(trainingMaterialId)).thenReturn(Optional.of(trainingMaterial));
//        Mockito.when(bu.getCustomer()).thenReturn("my-avatar");
//
//        // Act
//        TrainingMaterialsDto result = trainingMaterialsService.getTrainingMaterialDataById(trainingMaterialId);
//
//        // Assert
//        Assert.assertEquals(s3Location, result.getS3Location());
//        Assert.assertArrayEquals(expectedData, result.getTrainingMaterialData());
//        Mockito.verify(s3Sevice).getObject(Mockito.anyString(), Mockito.anyString());
//    }

//    @Test
//    public void testGetTrainingMaterialDataById() throws ResourceNotFoundException {
//        // Arrange
//        Long trainingMaterialId = 1L;
//        TrainingMaterials trainingMaterials = new TrainingMaterials();
//        trainingMaterials.setId(trainingMaterialId);
//        trainingMaterials.setS3Location("ok3");
//
//        Mockito.when(trainingMaterialsRepositoty.findById(trainingMaterialId)).thenReturn(Optional.of(trainingMaterials));
//        TrainingMaterialsDto dto = trainingMaterialsService.getTrainingMaterialDataById(trainingMaterialId);
//        assertTrue(trainingMaterials.getS3Location().equals(dto.getS3Location()));
//    }
    @Test
    public void testGetTrainingMaterialDataById_WhenTrainingMaterialDoesNotExist() throws ResourceNotFoundException {
        // Arrange
        Long trainingMaterialId = 1L;

        Mockito.when(trainingMaterialsRepositoty.findById(trainingMaterialId)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> trainingMaterialsService.getTrainingMaterialDataById(trainingMaterialId));

        // Assert (Expecting ResourceNotFoundException)
    }

    @Test
    public void testUpdateTrainingMaterials_WhenTrainingMaterialsExist() {
        // Arrange
        Long contentId = 1L;
        String newTitle = "New Title";

        TrainingMaterials trainingMaterials = new TrainingMaterials();
        trainingMaterials.setId(contentId);

        Mockito.when(trainingMaterialsRepositoty.findById(contentId)).thenReturn(Optional.of(trainingMaterials));
        Mockito.when(trainingMaterialsRepositoty.save(Mockito.any(TrainingMaterials.class))).thenReturn(trainingMaterials);

        // Act
        TrainingMaterialsDto result = trainingMaterialsService.updateTrainingMaterials(contentId, newTitle);

        // Assert
        Assert.assertNotNull(result);
        Assert.assertEquals(newTitle, result.getTitle());
        Mockito.verify(trainingMaterialsRepositoty).findById(contentId);
        Mockito.verify(trainingMaterialsRepositoty).save(Mockito.any(TrainingMaterials.class));
    }

    @Test
    public void testUpdateTrainingMaterials_WhenTrainingMaterialsDoNotExist() {
        // Arrange
        Long contentId = 1L;
        String newTitle = "New Title";

        Mockito.when(trainingMaterialsRepositoty.findById(contentId)).thenReturn(Optional.empty());

        // Act
        TrainingMaterialsDto result = trainingMaterialsService.updateTrainingMaterials(contentId, newTitle);

        // Assert
        Assert.assertNull(result);
        Mockito.verify(trainingMaterialsRepositoty).findById(contentId);
        Mockito.verify(trainingMaterialsRepositoty, Mockito.never()).save(Mockito.any(TrainingMaterials.class));
    }

    @Test
    public void testDeleteMaterial_WhenMaterialExists() {
        // Arrange
        Long materialId = 1L;
        TrainingMaterials trainingMaterials = new TrainingMaterials();
        trainingMaterials.setId(materialId);

        Mockito.when(trainingMaterialsRepositoty.findById(materialId)).thenReturn(Optional.of(trainingMaterials));

        // Act
        trainingMaterialsService.deleteMaterial(materialId);

        // Assert
        Mockito.verify(trainingMaterialsRepositoty).findById(materialId);
        Mockito.verify(trainingMaterialsRepositoty).delete(trainingMaterials);
    }

    @Test
    public void testDeleteMaterial_WhenMaterialDoesNotExist() {
        // Arrange
        Long materialId = 1L;

        Mockito.when(trainingMaterialsRepositoty.findById(materialId)).thenReturn(Optional.empty());

        assertThrows(MaterialNotFoundException.class, () -> trainingMaterialsService.deleteMaterial(materialId));

    }

    @Test
    public void testGetMaterialByContentId_WhenMaterialsExist() {
        // Arrange
        Long contentId = 1L;
        TrainingMaterials material1 = new TrainingMaterials();
        material1.setId(1L);
        material1.setTitle("Material 1");
        material1.setS3Location("s3://material1");
        // Set other properties

        TrainingMaterials material2 = new TrainingMaterials();
        material2.setId(2L);
        material2.setTitle("Material 2");
        material2.setS3Location("s3://material2");
        // Set other properties

        List<TrainingMaterials> materialsList = new ArrayList<>();
        materialsList.add(material1);
        materialsList.add(material2);

        Mockito.when(trainingMaterialsRepositoty.findByTrainingContentId(contentId)).thenReturn(materialsList);

        // Act
        List<TrainingMaterialsDto> result = trainingMaterialsService.getMaterialByContentId(contentId);

        // Assert
        Assert.assertEquals(2, result.size());
        // Add more assertions to verify the content of TrainingMaterialsDto objects
    }

    @Test
    public void testGetMaterialByContentId_WhenNoMaterialsExist() {
        // Arrange
        Long contentId = 1L;
        Mockito.when(trainingMaterialsRepositoty.findByTrainingContentId(contentId)).thenReturn(Collections.emptyList());

        // Act
        List<TrainingMaterialsDto> result = trainingMaterialsService.getMaterialByContentId(contentId);

        // Assert
        Assert.assertTrue(result.isEmpty());
    }

}
