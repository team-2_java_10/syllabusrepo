package com.fams.syllabus_repository.Service.Impl;
import com.fams.syllabus_repository.dto.AssignmentShemeDto;
import com.fams.syllabus_repository.dto.OtherSyllabusCreateUpdateDto;
import com.fams.syllabus_repository.dto.OtherSyllabusDto;
import com.fams.syllabus_repository.entity.AssignmentSheme;
import com.fams.syllabus_repository.entity.OtherSyllabus;
import com.fams.syllabus_repository.entity.Syllabus;
import com.fams.syllabus_repository.exceptions.OtherSyllabusNotFoundException;
import com.fams.syllabus_repository.repository.AssignmentShemeRepository;
import com.fams.syllabus_repository.repository.OtherSyllabusRepository;
import com.fams.syllabus_repository.repository.SyllabusRepository;
import com.fams.syllabus_repository.repository.TimeAllocationRepositoty;
import com.fams.syllabus_repository.service.impl.OtherSyllabusImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
@ExtendWith(MockitoExtension.class)
public class OtherSyllabusServiceImplTest {
    @Mock
    private TimeAllocationRepositoty timeAllocationRepositoty;

    @Mock
    private SyllabusRepository syllabusRepository;

    @Mock
    private AssignmentShemeRepository assignmentShemeRepository;

    @Mock
    private OtherSyllabusRepository otherSyllabusRepository;

    @InjectMocks
    private OtherSyllabusImpl otherSyllabusService;

    @Test
    void testGetOrderSyllabusBySyllabusId() {
        // Arrange
        Long syllabusId = 1L;

        // Mock data
        OtherSyllabus otherSyllabus = new OtherSyllabus();
        otherSyllabus.setTraining("Training");
        otherSyllabus.setReTest("ReTest");
        otherSyllabus.setMarking("Marking");
        otherSyllabus.setWaiverCriteria("Waiver");
        otherSyllabus.setOther("Other");

        when(otherSyllabusRepository.findBySyllabusId(syllabusId))
                .thenReturn(Collections.singletonList(otherSyllabus));

        // Act
        List<OtherSyllabusDto> result = otherSyllabusService.getOrderSyllabusBySyllabusId(syllabusId);

        // Assert
        assertEquals(1, result.size());
        OtherSyllabusDto dto = result.get(0);
        assertEquals("Training", dto.getTraining());
        assertEquals("ReTest", dto.getReTest());
        assertEquals("Marking", dto.getMarking());
        assertEquals("Waiver", dto.getWaiverCriteria());
        assertEquals("Other", dto.getOther());
    }

    @Test
    void testCreateOrderSyllabus() {
        // Arrange
        Long syllabusId = 1L;
        OtherSyllabusDto otherSyllabusDto = new OtherSyllabusDto();
        otherSyllabusDto.setTraining("Training");
        otherSyllabusDto.setReTest("ReTest");
        otherSyllabusDto.setMarking("Marking");
        otherSyllabusDto.setWaiverCriteria("WaiverCriteria");
        otherSyllabusDto.setOther("Other");

        Syllabus syllabus = new Syllabus();
        syllabus.setId(syllabusId);

        when(syllabusRepository.findById(syllabusId)).thenReturn(java.util.Optional.of(syllabus));
        when(otherSyllabusRepository.save(any(OtherSyllabus.class))).thenAnswer(invocation -> {
            OtherSyllabus savedOtherSyllabus = invocation.getArgument(0);
            savedOtherSyllabus.setId(1L); // Simulate ID generation
            return savedOtherSyllabus;
        });

        // Act
        OtherSyllabusDto resultDto = otherSyllabusService.createOrderSyllabus(syllabusId, otherSyllabusDto);

        // Assert
        assertNotNull(resultDto);
        assertNotNull(resultDto.getId()); // Ensure ID is not null
        assertEquals(otherSyllabusDto.getTraining(), resultDto.getTraining());
        assertEquals(otherSyllabusDto.getReTest(), resultDto.getReTest());
        assertEquals(otherSyllabusDto.getMarking(), resultDto.getMarking());
        assertEquals(otherSyllabusDto.getWaiverCriteria(), resultDto.getWaiverCriteria());
        assertEquals(otherSyllabusDto.getOther(), resultDto.getOther());

        // Verify repository method invocations
        verify(syllabusRepository, times(1)).findById(syllabusId);
        verify(otherSyllabusRepository, times(1)).save(any(OtherSyllabus.class));
    }

    @Test
    void testUpdateOtherSyllabusBySyllabusId() {
        // Arrange
        Long syllabusId = 1L;
        OtherSyllabusDto otherSyllabusDto = new OtherSyllabusDto();
        otherSyllabusDto.setId(1L); // Assuming the ID is set in the DTO

        Syllabus syllabus = new Syllabus();
        syllabus.setId(syllabusId);

        OtherSyllabus existingOtherSyllabus = new OtherSyllabus();
        existingOtherSyllabus.setId(1L);
        existingOtherSyllabus.setSyllabus(syllabus);

        when(syllabusRepository.findById(syllabusId)).thenReturn(java.util.Optional.of(syllabus));
        when(otherSyllabusRepository.findById(otherSyllabusDto.getId())).thenReturn(java.util.Optional.of(existingOtherSyllabus));
        when(otherSyllabusRepository.save(any(OtherSyllabus.class))).thenAnswer(invocation -> invocation.getArgument(0));

        // Act
        OtherSyllabusDto resultDto = otherSyllabusService.updateOtherSyllabusBySyllabusId(syllabusId, otherSyllabusDto);

        // Assert
        assertNotNull(resultDto);
        assertEquals(otherSyllabusDto.getId(), resultDto.getId()); // Ensure the ID remains the same

        // Verify repository method invocations
        verify(syllabusRepository, times(1)).findById(syllabusId);
        verify(otherSyllabusRepository, times(1)).findById(otherSyllabusDto.getId());
        verify(otherSyllabusRepository, times(1)).save(any(OtherSyllabus.class));
    }

    @Test
    void testUpdateOtherSyllabus_NotFoundBySyllabus() {
        // Arrange
        Long syllabusId = 1L;
        OtherSyllabusDto otherSyllabusDto = new OtherSyllabusDto();
        otherSyllabusDto.setId(1L); // Assuming the ID is set in the DTO

        Syllabus syllabus = new Syllabus();
        syllabus.setId(syllabusId);
        Syllabus otherSyllabus = new Syllabus();
        otherSyllabus.setId(2L);
        OtherSyllabus existingOtherSyllabus = new OtherSyllabus();
        existingOtherSyllabus.setId(1L);
        existingOtherSyllabus.setSyllabus(otherSyllabus);

        when(syllabusRepository.findById(syllabusId)).thenReturn(java.util.Optional.of(syllabus));
        when(otherSyllabusRepository.findById(otherSyllabusDto.getId())).thenReturn(java.util.Optional.of(existingOtherSyllabus));

        assertThrows(OtherSyllabusNotFoundException.class,()-> otherSyllabusService.updateOtherSyllabusBySyllabusId(syllabusId, otherSyllabusDto));

        // Verify repository method invocations
        verify(syllabusRepository, times(1)).findById(syllabusId);
        verify(otherSyllabusRepository, times(1)).findById(otherSyllabusDto.getId());
        verify(otherSyllabusRepository, times(0)).save(any(OtherSyllabus.class));
    }

    @Test
    void testSaveOrUpdateOtherSyllabus_UpdateExisting() {
        // Arrange
        Long syllabusId = 1L;
        OtherSyllabusDto otherSyllabusDto = new OtherSyllabusDto();
        otherSyllabusDto.setId(1L); // Assuming the ID is set in the DTO

        Syllabus syllabus = new Syllabus();
        syllabus.setId(syllabusId);

        OtherSyllabus existingOtherSyllabus = new OtherSyllabus();
        existingOtherSyllabus.setId(1L);
        existingOtherSyllabus.setSyllabus(syllabus);

        when(syllabusRepository.findById(syllabusId)).thenReturn(java.util.Optional.of(syllabus));
        when(otherSyllabusRepository.findById(otherSyllabusDto.getId())).thenReturn(java.util.Optional.of(existingOtherSyllabus));
        when(otherSyllabusRepository.save(any(OtherSyllabus.class))).thenAnswer(invocation -> invocation.getArgument(0));

        // Act
        OtherSyllabusDto resultDto = otherSyllabusService.saveOrUpdateOtherSyllabus(syllabusId, otherSyllabusDto);

        // Assert
        assertNotNull(resultDto);
        assertEquals(otherSyllabusDto.getId(), resultDto.getId()); // Ensure the ID remains the same

        // Verify repository method invocations
        verify(syllabusRepository, times(1)).findById(syllabusId);
        verify(otherSyllabusRepository, times(1)).findById(otherSyllabusDto.getId());
        verify(otherSyllabusRepository, times(1)).save(any(OtherSyllabus.class));
    }


    @Test
    void testSaveOrUpdateOtherSyllabus_NotFoundBySyllabus() {
        // Arrange
        Long syllabusId = 1L;
        OtherSyllabusDto otherSyllabusDto = new OtherSyllabusDto();
        otherSyllabusDto.setId(1L); // Assuming the ID is set in the DTO

        Syllabus syllabus = new Syllabus();
        syllabus.setId(syllabusId);
        Syllabus otherSyllabus = new Syllabus();
        otherSyllabus.setId(2L);
        OtherSyllabus existingOtherSyllabus = new OtherSyllabus();
        existingOtherSyllabus.setId(1L);
        existingOtherSyllabus.setSyllabus(otherSyllabus);

        when(syllabusRepository.findById(syllabusId)).thenReturn(java.util.Optional.of(syllabus));
        when(otherSyllabusRepository.findById(otherSyllabusDto.getId())).thenReturn(java.util.Optional.of(existingOtherSyllabus));

        assertThrows(OtherSyllabusNotFoundException.class,()-> otherSyllabusService.saveOrUpdateOtherSyllabus(syllabusId, otherSyllabusDto));

        // Verify repository method invocations
        verify(syllabusRepository, times(1)).findById(syllabusId);
        verify(otherSyllabusRepository, times(1)).findById(otherSyllabusDto.getId());
        verify(otherSyllabusRepository, times(0)).save(any(OtherSyllabus.class));
    }
    @Test
    void testSaveOrUpdateOtherSyllabus_SaveNew() {
        // Arrange
        Long syllabusId = 1L;
        OtherSyllabusDto otherSyllabusDto = new OtherSyllabusDto();
        otherSyllabusDto.setId(0L);
        OtherSyllabus otherSyllabus = new OtherSyllabus();
        otherSyllabus.setId(any());
        Syllabus syllabus = new Syllabus();
        syllabus.setId(syllabusId);
        when(syllabusRepository.findById(syllabusId)).thenReturn(Optional.of(syllabus));
        when(otherSyllabusRepository.save(any())).thenReturn(otherSyllabus);
        otherSyllabusService.saveOrUpdateOtherSyllabus(syllabusId, otherSyllabusDto);
        // Verify repository method invocations
        verify(syllabusRepository, times(1)).findById(syllabusId);
        verify(otherSyllabusRepository, times(1)).save(any(OtherSyllabus.class));
    }
    @Test
    public void testCreateOrUpdateSchemesBySyllabusId() {
        // Arrange
        Long syllabusId = 1L;
        OtherSyllabusCreateUpdateDto dto = new OtherSyllabusCreateUpdateDto();
        AssignmentShemeDto assignmentSchemeDto = new AssignmentShemeDto();
        OtherSyllabusDto otherSyllabusDto = new OtherSyllabusDto();
        dto.setAssignmentShemeDtos(assignmentSchemeDto);
        dto.setOtherSyllabusDtos(otherSyllabusDto);

        Syllabus syllabus = new Syllabus();
        Mockito.when(syllabusRepository.findById(syllabusId)).thenReturn(Optional.of(syllabus));

        AssignmentSheme assignmentScheme = new AssignmentSheme();
        Mockito.when(assignmentShemeRepository.findBySyllabus(syllabus)).thenReturn(assignmentScheme);

        OtherSyllabus otherSyllabus = new OtherSyllabus();
        Mockito.when(otherSyllabusRepository.findBySyllabus(syllabus)).thenReturn(otherSyllabus);

        // Act
        OtherSyllabusCreateUpdateDto resultDto = otherSyllabusService.createOrUpdateSchemesBySyllabusId(syllabusId, dto);

        // Assert
        assertEquals(LocalDate.now(), assignmentScheme.getModifyDate());
        assertEquals(LocalDate.now(), otherSyllabus.getModifyDate());
    }
    @Test
    public void testCreateOrUpdateSchemesBySyllabusId_WithNull() {
        // Arrange
        Long syllabusId = 1L;
        OtherSyllabusCreateUpdateDto dto = new OtherSyllabusCreateUpdateDto();
        AssignmentShemeDto assignmentSchemeDto = new AssignmentShemeDto();
        OtherSyllabusDto otherSyllabusDto = new OtherSyllabusDto();
        dto.setAssignmentShemeDtos(assignmentSchemeDto);
        dto.setOtherSyllabusDtos(otherSyllabusDto);

        Syllabus syllabus = new Syllabus();
        Mockito.when(syllabusRepository.findById(syllabusId)).thenReturn(Optional.of(syllabus));

        Mockito.when(assignmentShemeRepository.findBySyllabus(syllabus)).thenReturn(null);

        Mockito.when(otherSyllabusRepository.findBySyllabus(syllabus)).thenReturn(null);

        // Act
        otherSyllabusService.createOrUpdateSchemesBySyllabusId(syllabusId, dto);
        verify(assignmentShemeRepository, times(1)).findBySyllabus(syllabus);
        verify(otherSyllabusRepository, times(1)).findBySyllabus(syllabus);

    }

    @Test
    void createOtherSyllabus_WithExistingSyllabus_ShouldReturnOtherSyllabusDto() {
        // Arrange
        Long syllabusId = 1L;
        OtherSyllabusDto otherSyllabusDto = new OtherSyllabusDto();
        // Set properties for otherSyllabusDto

        Syllabus syllabus = new Syllabus();
        // Set properties for syllabus

        OtherSyllabus otherSyllabus = new OtherSyllabus();
        // Set properties for otherSyllabus

        when(syllabusRepository.findById(syllabusId)).thenReturn(Optional.of(syllabus));
        when(otherSyllabusRepository.save(any(OtherSyllabus.class))).thenReturn(otherSyllabus);

        // Act
        OtherSyllabusDto result = otherSyllabusService.createOrderSyllabus(syllabusId, otherSyllabusDto);

        // Assert
        assertEquals(otherSyllabus.getId(), result.getId());
        // Assert other properties of result against otherSyllabusDto

        verify(syllabusRepository, times(1)).findById(syllabusId);
        verify(otherSyllabusRepository, times(1)).save(any(OtherSyllabus.class));
    }

    @Test
    void createOtherSyllabus_WithNonExistingSyllabus_ShouldThrowException() {
        // Arrange
        Long syllabusId = 1L;
        OtherSyllabusDto otherSyllabusDto = new OtherSyllabusDto();
        // Set properties for otherSyllabusDto

        when(syllabusRepository.findById(syllabusId)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(OtherSyllabusNotFoundException.class, () -> {
            otherSyllabusService.createOrderSyllabus(syllabusId, otherSyllabusDto);
        });

        verify(syllabusRepository, times(1)).findById(syllabusId);
        verify(otherSyllabusRepository, never()).save(any(OtherSyllabus.class));
    }
}
