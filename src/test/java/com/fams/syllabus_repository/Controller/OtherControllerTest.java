package com.fams.syllabus_repository.Controller;

import com.fams.syllabus_repository.controller.OtherController;
import com.fams.syllabus_repository.dto.OtherSyllabusCreateUpdateDto;
import com.fams.syllabus_repository.service.OtherSyllabusService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;


public class OtherControllerTest {
    @InjectMocks
    private OtherController otherController;

    @Mock
    private OtherSyllabusService otherSyllabusService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }
    @Test
    public void testCreateOrUpdateSchemesBySyllabusId() {
        OtherSyllabusCreateUpdateDto dto = new OtherSyllabusCreateUpdateDto();
        dto.setId(1L);
        OtherSyllabusCreateUpdateDto createdOrUpdated = new OtherSyllabusCreateUpdateDto();

        when(otherSyllabusService.createOrUpdateSchemesBySyllabusId(dto.getId(), dto)).thenReturn(createdOrUpdated);

        ResponseEntity<OtherSyllabusCreateUpdateDto> responseEntity = otherController.createOrUpdateSchemesBySyllabusId(dto);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(createdOrUpdated, responseEntity.getBody());
    }
}
