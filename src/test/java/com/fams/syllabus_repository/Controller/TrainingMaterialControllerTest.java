package com.fams.syllabus_repository.Controller;

import com.fams.syllabus_repository.controller.TrainingMaterialController;
import com.fams.syllabus_repository.dto.TrainingMaterialsDto;
import com.fams.syllabus_repository.entity.TrainingMaterials;
import com.fams.syllabus_repository.exceptions.MaterialNotFoundException;
import com.fams.syllabus_repository.exceptions.ResourceNotFoundException;
import com.fams.syllabus_repository.exceptions.SyllabusNotFoundException;
import com.fams.syllabus_repository.repository.TrainingMaterialsRepositoty;
import com.fams.syllabus_repository.request.RequestIdDto;
import com.fams.syllabus_repository.response.ResponseObject;
import com.fams.syllabus_repository.response.ResponseObjectDto;
import com.fams.syllabus_repository.service.impl.TrainingMaterialsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class TrainingMaterialControllerTest {
    private MockMvc mockMvc;

    @InjectMocks
    private TrainingMaterialController trainingMaterialController;

    @Mock
    private TrainingMaterialsServiceImpl trainingMaterialsService;

    @Mock
    private TrainingMaterialsRepositoty trainingMaterialsRepositoty;



    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(trainingMaterialController).build();
    }

    @Test
    public void testMaterialDetailSuccess() {
        RequestIdDto request = new RequestIdDto();
        Long id = 1L;
        request.setId(id);

        List<TrainingMaterialsDto> materials = new ArrayList<>();

        when(trainingMaterialsService.getMaterialByContentId(id)).thenReturn(materials);

        ResponseObject<Object> response = trainingMaterialController.materialDetail(request);

        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
        assertEquals("Material successfully", response.getMessage());
        assertEquals(materials, response.getData());

        verify(trainingMaterialsService, times(1)).getMaterialByContentId(id);
    }

    @Test
    public void testMaterialDetailSyllabusNotFoundException() {
        RequestIdDto request = new RequestIdDto();
        Long id = 1L;
        request.setId(id);

        when(trainingMaterialsService.getMaterialByContentId(id)).thenThrow(new SyllabusNotFoundException("Syllabus not found"));

        ResponseObject<Object> response = trainingMaterialController.materialDetail(request);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatusCode());
        assertEquals("Error syllabus", response.getMessage());

        verify(trainingMaterialsService, times(1)).getMaterialByContentId(id);
    }

    @Test
    public void testUploadTrainingMaterialToS3Success() {
        Long trainingContentId = 1L;
        MultipartFile file = mock(MultipartFile.class);
        String createdBy = "user1";
        String modifyBy = "user2";
        when(trainingMaterialsService.uploadTrainingMaterialToS3(trainingContentId, file, createdBy, modifyBy))
                .thenReturn("s3-key");

        ResponseEntity<String> response = trainingMaterialController.uploadTrainingMaterialToS3(trainingContentId, file, createdBy, modifyBy);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Training material uploaded with S3 key: s3-key", response.getBody());
    }

    @Test
    public void testUploadTrainingMaterialToS3Failure() {
        Long trainingContentId = 1L;
        MultipartFile file = mock(MultipartFile.class);
        String createdBy = "user1";
        String modifyBy = "user2";
        when(trainingMaterialsService.uploadTrainingMaterialToS3(trainingContentId, file, createdBy, modifyBy))
                .thenThrow(new RuntimeException("An error occurred"));

        ResponseEntity<String> response = trainingMaterialController.uploadTrainingMaterialToS3(trainingContentId, file, createdBy, modifyBy);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals("An error occurred: An error occurred", response.getBody());
    }

    @Test
    public void testGetTrainingMaterialFromS3Success() throws ResourceNotFoundException {
        Long id = 1L;
        TrainingMaterialsDto trainingMaterialResponse = new TrainingMaterialsDto();
        when(trainingMaterialsService.getTrainingMaterialDataById(id))
                .thenReturn(trainingMaterialResponse);

        ResponseEntity<ResponseObjectDto> response = trainingMaterialController.getTrainingMaterialFromS3(id);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(0, response.getBody().getCode());
        assertEquals("success", response.getBody().getMessage());
        assertEquals(trainingMaterialResponse, response.getBody().getData());
    }

    @Test
    public void testGetTrainingMaterialFromS3ResourceNotFound() throws ResourceNotFoundException {
        Long id = 1L;
        when(trainingMaterialsService.getTrainingMaterialDataById(id))
                .thenThrow(new ResourceNotFoundException("Training material not found"));

        ResponseEntity<ResponseObjectDto> response = trainingMaterialController.getTrainingMaterialFromS3(id);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        ResponseObjectDto responseObject = response.getBody();
        assertNotNull(responseObject);
        assertEquals(1, responseObject.getCode());
        assertEquals("Resource not found", responseObject.getMessage());
        assertNull(responseObject.getData());
    }

    @Test
    public void testDownloadFromS3Success() {
        Long resourceId = 1L;
        byte[] fileData = new byte[] { 1, 2, 3, 4, 5 }; // Sample file data

        TrainingMaterials resource = new TrainingMaterials();
        resource.setTitle("SampleFile.docx");

        when(trainingMaterialsService.getTrainingMaterialFromS3(resourceId)).thenReturn(fileData);
        when(trainingMaterialsRepositoty.findById(resourceId)).thenReturn(Optional.of(resource));

        ResponseEntity<byte[]> response = trainingMaterialController.downloadFromS3(resourceId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        HttpHeaders headers = response.getHeaders();
        assertEquals("attachment; filename=SampleFile.docx", headers.getFirst(HttpHeaders.CONTENT_DISPOSITION));
        assertEquals(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.wordprocessingml.document"), headers.getContentType());
        assertEquals(fileData, response.getBody());
    }

    @Test
    public void testDownloadFromS3NotFound() {
        Long resourceId = 1L;

        when(trainingMaterialsService.getTrainingMaterialFromS3(resourceId)).thenReturn(null);

        ResponseEntity<byte[]> response = trainingMaterialController.downloadFromS3(resourceId);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testDeleteMaterial_Success() throws Exception {
        // Prepare test data
        Long materialId = 1L;
        RequestIdDto requestIdDto = new RequestIdDto(materialId);

        // Mock the behavior of trainingMaterialsService.deleteMaterial
//        doNothing().when(trainingMaterialsService).deleteMaterial(materialId);

        ResponseEntity<String> response = trainingMaterialController.deleteMaterial(requestIdDto);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Material with ID " + requestIdDto.getId() + " has been deleted", response.getBody());
        // Verify that trainingMaterialsService.deleteMaterial was called with the expected parameter
        verify(trainingMaterialsService, times(1)).deleteMaterial(materialId);
    }

    @Test
    public void testDeleteMaterial_MaterialNotFound() throws Exception {
        Long materialId = 1L;
        RequestIdDto requestIdDto = new RequestIdDto(materialId);

        // Mock the behavior of trainingMaterialsService.deleteMaterial
        doThrow(new MaterialNotFoundException("Material not found for ID: " + materialId)).when(trainingMaterialsService).deleteMaterial(materialId);

        // Perform the request and verify the response
        ResponseEntity<String> response = trainingMaterialController.deleteMaterial(requestIdDto);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Material not found for ID: " + materialId, response.getBody());
        // Verify that trainingMaterialsService.deleteMaterial was called with the expected parameter
        verify(trainingMaterialsService, times(1)).deleteMaterial(materialId);
    }

    @Test
    public void testDeleteMaterial_InternalServerError() throws Exception {
        // Prepare test data
        Long materialId = 1L;
        RequestIdDto requestIdDto = new RequestIdDto(materialId);

        // Mock the behavior of trainingMaterialsService.deleteMaterial
        doThrow(new RuntimeException("Internal server error")).when(trainingMaterialsService).deleteMaterial(materialId);

        // Perform the request and verify the response
        ResponseEntity<String> response = trainingMaterialController.deleteMaterial(requestIdDto);

        // Verify that trainingMaterialsService.deleteMaterial was called with the expected parameter
        verify(trainingMaterialsService, times(1)).deleteMaterial(materialId);
    }
}
