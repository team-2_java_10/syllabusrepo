package com.fams.syllabus_repository.Controller;

import com.fams.syllabus_repository.controller.SyllabusController;
import com.fams.syllabus_repository.dto.*;
import com.fams.syllabus_repository.entity.Syllabus;
import com.fams.syllabus_repository.exceptions.SyllabusNotFoundException;
import com.fams.syllabus_repository.repository.SyllabusRepository;
import com.fams.syllabus_repository.repository.TrainingMaterialsRepositoty;
import com.fams.syllabus_repository.request.RequestIdDto;
import com.fams.syllabus_repository.response.ResponseObject;
import com.fams.syllabus_repository.service.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class SyllabusControllerTest {

    private MockMvc mockMvc;
    @InjectMocks
    private SyllabusController syllabusController;

    @Mock
    private SyllabusService syllabusService;
    @Mock
    private SyllabusRepository syllabusRepository;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(syllabusController).build();
    }
    private static final long SAMPLE_SYLLABUS_ID = 1L;



    @Test
    public void testGetAllSyllabus(){
        int pageNo = 1;
        int pageSize = 10;
        String search = "keyword";

        // Create a mock PagingSyllabusDto
        PagingSyllabusDto request = new PagingSyllabusDto();
        request.setPageNo(pageNo);
        request.setPageSize(pageSize);
        request.setSearch(search);

        // Create a mock Page<Syllabus>
        Page<Syllabus> syllabusPage = new PageImpl<>(new ArrayList<>());

        // Create a mock PagingSyllabusDto
        PagingSyllabusDto expectedResponse = new PagingSyllabusDto();
        expectedResponse.setContent(new ArrayList<>());
        expectedResponse.setPageNo(pageNo);
        expectedResponse.setPageSize(pageSize);
        expectedResponse.setTotalElements(syllabusPage.getTotalElements());
        expectedResponse.setTotalPages(syllabusPage.getTotalPages());
        expectedResponse.setLast(syllabusPage.isLast());
        expectedResponse.setSearch(search);

        // Mock the behavior of syllabusService.getAllSyllabusAndSearch()
        when(syllabusService.getAllSyllabusAndSearch(pageNo, pageSize, search)).thenReturn(expectedResponse);

        // Act
        PagingSyllabusDto actualResponse = syllabusController.getAllSyllabus(request);

        // Assert
        assertEquals(expectedResponse, actualResponse);
    }


    @Test
    public void testSyllabusDetailSuccess() throws SyllabusNotFoundException {
        // Arrange
        Long id = 1L;
        SyllabusDetailDto expectedDto = new SyllabusDetailDto(/* Initialize with test data */);
        RequestIdDto request = new RequestIdDto(id);

        when(syllabusService.getSyllabusById(id)).thenReturn(expectedDto);

        // Act
        ResponseObject<Object> response = syllabusController.syllabusDetail(request);

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
        assertEquals("Syllabus successfully", response.getMessage());
        assertEquals(expectedDto, response.getData());
    }

    @Test
    public void testSyllabusDetailSyllabusNotFoundException() throws SyllabusNotFoundException {
        // Arrange
        Long id = 2L;
        RequestIdDto request = new RequestIdDto(id);

        when(syllabusService.getSyllabusById(id)).thenThrow(new SyllabusNotFoundException("Syllabus not found"));

        // Act
        ResponseObject<Object> response = syllabusController.syllabusDetail(request);

        // Assert
        assertNotNull(response);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatusCode());
        assertEquals("Error syllabus", response.getMessage());
    }

    @Test
    public void testUpdateGeneral() {
        // Tạo một đối tượng SyllabusDto mẫu để sử dụng trong kiểm thử
        SyllabusDto requestDto = new SyllabusDto();
        requestDto.setId(1L);
        requestDto.setSyllabusName("Updated Name");

        SyllabusDto expectedResponse = new SyllabusDto();
        expectedResponse.setId(1L);
        expectedResponse.setSyllabusName("Updated Name");

        // Mô phỏng behavior cho syllabusService.createAndUpdate
        when(syllabusService.createAndUpdate(requestDto)).thenReturn(expectedResponse);

        // Gọi phương thức cần được kiểm thử
        ResponseEntity<SyllabusDto> responseEntity = syllabusController.updateGeneral(requestDto);

        // Kiểm tra xem phản hồi có đúng không
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    public void testDuplicateSyllabus() {
        Long id = 1L;
        RequestIdDto requestIdDto = new RequestIdDto();
        requestIdDto.setId(id);

        SyllabusDto duplicatedSyllabus = new SyllabusDto();
        duplicatedSyllabus.setId(id);

        when(syllabusService.duplicateSyllabus(id)).thenReturn(duplicatedSyllabus);

        ResponseEntity<SyllabusDto> response = syllabusController.duplicateSyllabus(requestIdDto);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(id, response.getBody().getId());
    }
    @Test
    public void testDuplicateSyllabus_NotFound() {
        Long id = 1L; // Đây là một giá trị id giả định, bạn có thể thay đổi nó tùy theo trường hợp kiểm tra

        // Tạo một đối tượng RequestIdDto với giá trị id
        RequestIdDto requestIdDto = new RequestIdDto();
        requestIdDto.setId(id);

        // Mô phỏng việc gọi syllabusService.duplicateSyllabus và trả về giá trị null
        Mockito.when(syllabusService.duplicateSyllabus(id)).thenReturn(null);

        // Gọi phương thức cần được kiểm thử
        ResponseEntity<SyllabusDto> responseEntity = syllabusController.duplicateSyllabus(requestIdDto);

        // Kiểm tra xem phản hồi có đúng không
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

    @Test
    public void testImportSyllabusFromCSV() throws Exception {
        // Prepare test data
        MockMultipartFile file = new MockMultipartFile("file", "test.csv", "text/csv", "SyllabusName,Version,AttendeeNumber,Duration,TechnicalRequirement,CourseObjectives,Level\nSyllabus Name Test,V1,100,5,TechnicalRequirement1,CourseObjectives1,Level1".getBytes());
        String createdBy = "John Doe";
        String modifiedBy = "Jane Doe";
        String option = "someOption";

        // Mock the behavior of syllabusService.importSyllabusFromCSV
        doNothing().when(syllabusService).importSyllabusFromCSV(any(), eq(createdBy), eq(modifiedBy), eq(option));

        ResponseEntity<String> responseEntity = syllabusController.importSyllabusFromCSV(file, createdBy, modifiedBy, option);

        // Verify that syllabusService.importSyllabusFromCSV was called with the expected parameters
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void testDeleteSyllabusByIdSuccess() {
        // Mock the successful deletion scenario
        doNothing().when(syllabusService).deleteSyllabus(SAMPLE_SYLLABUS_ID);

        // Perform the request and verify the response
        ResponseEntity<String> responseEntity = syllabusController.deleteSyllabusById(createRequestIdDto(SAMPLE_SYLLABUS_ID));

        // Verify the expected behavior
        verify(syllabusService, times(1)).deleteSyllabus(SAMPLE_SYLLABUS_ID);
        assertResponseEntityString(responseEntity, "Syllabus deleted successfully", HttpStatus.OK);
    }


    @Test
    public void testDeleteSyllabusByIdNotFound() {
        // Mock the scenario where SyllabusNotFoundException is thrown
        doThrow(new SyllabusNotFoundException("Syllabus not found")).when(syllabusService).deleteSyllabus(SAMPLE_SYLLABUS_ID);

        // Perform the request and verify the response
        ResponseEntity<String> responseEntity = syllabusController.deleteSyllabusById(createRequestIdDto(SAMPLE_SYLLABUS_ID));

        // Verify the expected behavior
        verify(syllabusService, times(1)).deleteSyllabus(SAMPLE_SYLLABUS_ID);
        assertResponseEntityString(responseEntity, "Syllabus not found", HttpStatus.NOT_FOUND);
    }


    @Test
    public void testDeleteSyllabusByIdBadRequest() {
        // Mock the scenario where an exception is thrown
        doThrow(new RuntimeException("Some unexpected error")).when(syllabusService).deleteSyllabus(SAMPLE_SYLLABUS_ID);

        // Perform the request and verify the response
        ResponseEntity<String> responseEntity = syllabusController.deleteSyllabusById(createRequestIdDto(SAMPLE_SYLLABUS_ID));

        // Verify the expected behavior
        verify(syllabusService, times(1)).deleteSyllabus(SAMPLE_SYLLABUS_ID);
        assertResponseEntityString(responseEntity, "Failed to delete syllabus", HttpStatus.BAD_REQUEST);
    }

    @Test
    public void testGetAllForService() {

        when(syllabusService.getListSyllabusFOrService(new ArrayList<>())).thenReturn(new ArrayList<>());

        // Perform the request and verify the response
        ResponseEntity<List<SyllabusDto>> responseEntity = syllabusController.getAll(anyList());

        // Verify the expected behavior
        verify(syllabusService, times(1)).getListSyllabusFOrService(any());
    }

    private void assertResponseEntityString(ResponseEntity<String> responseEntity, String expectedBody, HttpStatus expectedStatus) {
        // Assert the response status and body
        assert responseEntity != null;
        assert responseEntity.getStatusCode() == expectedStatus;
        assert responseEntity.getBody().equals(expectedBody);
    }

    private RequestIdDto createRequestIdDto(long id) {
        RequestIdDto requestIdDto = new RequestIdDto();
        requestIdDto.setId(id);
        return requestIdDto;
    }


}

