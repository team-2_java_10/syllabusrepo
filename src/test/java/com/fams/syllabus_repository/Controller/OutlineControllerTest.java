package com.fams.syllabus_repository.Controller;

import com.fams.syllabus_repository.controller.OutlineController;
import com.fams.syllabus_repository.dto.DaySyllabusDto;
import com.fams.syllabus_repository.dto.OutlineRequestDto;
import com.fams.syllabus_repository.request.RequestIdDto;
import com.fams.syllabus_repository.service.TrainingContentService;
import com.fams.syllabus_repository.service.TrainingUnitService;
import com.fams.syllabus_repository.service.impl.DaySyllabusServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class OutlineControllerTest {
    @InjectMocks
    private OutlineController outlineController;
    @Mock
    private DaySyllabusServiceImpl daySyllabusService;
    @Mock
    private TrainingUnitService trainingUnitService;
    @Mock
    private TrainingContentService trainingContentService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }
    @Test
    public void testDeleteDaySyllabusSuccess() {
        RequestIdDto request = new RequestIdDto();
        Long dayId = 1L;
        request.setId(dayId);

        String result = outlineController.deleteDaySyllabus(request);

        assertEquals("success_view", result);
        verify(daySyllabusService).deleteDaySyllabusById(dayId);
    }

    @Test
    public void testDeleteDaySyllabusError() {
        RequestIdDto request = new RequestIdDto();
        Long dayId = 2L;
        request.setId(dayId);

        doThrow(RuntimeException.class).when(daySyllabusService).deleteDaySyllabusById(dayId);

        String result = outlineController.deleteDaySyllabus(request);

        assertEquals("error_view", result);
        verify(daySyllabusService).deleteDaySyllabusById(dayId);
    }
    @Test
    public void testUpdateCreateSyllabus() {
        Long id = 1L;
        List<DaySyllabusDto> daySyllabusDtos = new ArrayList<>();

        when(daySyllabusService.updateDaySyllabusList(id, daySyllabusDtos)).thenReturn(daySyllabusDtos);

        OutlineRequestDto updateRequest = new OutlineRequestDto();
        updateRequest.setId(id);
        updateRequest.setData(daySyllabusDtos);

        ResponseEntity<List<DaySyllabusDto>> responseEntity = outlineController.updateCreateSyllabus(updateRequest);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(daySyllabusDtos, responseEntity.getBody());
    }

    @Test
    public void testDeleteUnitSuccess() {
        RequestIdDto deleteRequest = new RequestIdDto();
        Long unitId = 1L;
        deleteRequest.setId(unitId);

        String result = outlineController.deleteUnit(deleteRequest);

        assertEquals("success_view", result);
        verify(trainingUnitService).deleteTrainingUnitById(unitId);
    }

    @Test
    public void testDeleteUnitError() {
        RequestIdDto deleteRequest = new RequestIdDto();
        Long unitId = 2L;
        deleteRequest.setId(unitId);

        doThrow(new RuntimeException("Error deleting unit")).when(trainingUnitService).deleteTrainingUnitById(anyLong());

        String result = outlineController.deleteUnit(deleteRequest);

        assertEquals("error_view", result);
        verify(trainingUnitService).deleteTrainingUnitById(unitId);
    }



    @Test
    public void testDeleteContentSuccess() {
        RequestIdDto deleteRequest = new RequestIdDto();
        Long contentId = 1L; // Set an example contentId
        deleteRequest.setId(contentId);

        String result = outlineController.deleteContent(deleteRequest);

        assertEquals("success_view", result);
        verify(trainingContentService).deleteTrainingContentById(contentId);
    }

    @Test
    public void testDeleteContentError() {
        RequestIdDto deleteRequest = new RequestIdDto();
        Long contentId = 2L;
        deleteRequest.setId(contentId);

        doThrow(DataIntegrityViolationException.class).when(trainingContentService).deleteTrainingContentById(contentId);

        String result = outlineController.deleteContent(deleteRequest);

        assertEquals("error_view", result);
        verify(trainingContentService).deleteTrainingContentById(contentId);
    }
}
