package com.fams.syllabus_repository.dto;

import com.fams.syllabus_repository.entity.Syllabus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TrainingUnitDto extends BaseDto{
    private String unitName;
    private String unitNumber;
}
